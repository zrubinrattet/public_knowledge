# SFMOMA's Public Knowledge

## Setup

Set this up like you would a normal WP install with a few configuration differences. This is a [multisite](https://codex.wordpress.org/Create_A_Network) so, if you're using MAMP, you will want to make an alias and use ports 80, 81, 443, 7443, 3006 & 11211. Make sure you update `wp-config.php` and `htaccess` accordingly to the documentation. You can get a dump of the database & the files in the uploads directory from WPEngine - ask @jaymollica for access if you don't have it. You also may need to get a self-signed ssl certificate working locally (this can be a pain). You should be able to find a lead on what will work for you [here](https://serverfault.com/questions/845766/generating-a-self-signed-cert-with-openssl-that-works-in-chrome-58)

## Use
`cd` into the theme directory and run `npm install` to get the front end task running environment built and `grunt` to initialize the task runner. You will need Node.js, NPM, Sass and Grunt CLI installed for this to work.