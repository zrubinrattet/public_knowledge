<?php 
/**
 * Template Name: Contact Page
 *
 */ 

get_header();

include(locate_template( 'modules/nav.php' ));

if( empty(get_field('disable_hero')) ){
	include(locate_template( 'modules/homehero.php' ));
}
?>
<div class="mainBackgroundColor">
	<?php
		include(locate_template( 'modules/contactform.php' ));
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>