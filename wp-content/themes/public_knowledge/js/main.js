
(function ( $, window, document, undefined ) {

	$(document).ready(function(){
		/**
		 * [Fade handles the fading between pages]
		 * @type {Object}
		 */
		var Fade = {
			/**
			 * [keydown tracks if a key is currently pressed]
			 * @type {Boolean}
			 */
			keydown : false,
			/**
			 * [links all the links that trigger/dont trigger the fade effect]
			 * @type {[type]}
			 */
			links : $('a:not(.em-calnav, .events-grid-item-content-ical, .nofade, .em-toggle, .slider-slide, #ctf *, .bloggrid-item-postcontainer-post a, .thestacksgrid-main-grid-post-textcontainer-share-link, .inpagenav-menu-item.backtotop)'),
			/**
			 * [_init entry point]
			 */
			_init : function(){
				// remove the fade class when the window loads or the pageshow event fires
				// pageshow is for safari which uses a back/forward cache
				// see https://stackoverflow.com/questions/158319/is-there-a-cross-browser-onload-event-when-clicking-the-back-button#answer-2218733

				var events = 'load pagenow';

				// checking if chrome or ie
				if( (/*@cc_on!@*/false || !!document.documentMode) || (!!window.chrome && !!window.chrome.webstore) ){
					// set load events
					events = 'load';
					// in case load event doesn't get fired and we're on windows
					setTimeout(function(){
						if( $('body').hasClass('fade') && $('body').hasClass('win') ){
							$('body').removeClass('fade');
						}
					}, 3000);

				}
				// $(window).on('load pageshow', function(){
				$(window).on(events, function(){
					$('body').removeClass('fade');
				});

				// listen for click events on the links
				Fade.links.on('click', Fade._clickHandler);
				// listen for keyup and keydown events
				$(window).on('keyup keydown', Fade._keyHandler);
			},
			/**
			 * [_keyHandler set keydown based on event type]
			 * @param  {obj} e the event object
			 */
			_keyHandler : function(e){
				if( e.type == 'keydown' ){
					Fade.keydown = true;
				}
				else if( e.type == 'keyup' ){
					Fade.keydown = false;
				}
			},
			/**
			 * [_clickHandler add fade class to body when click is done and when keys not pressed]
			 * @param  {obj} e the event object
			 */
			_clickHandler : function(e){
				if( e.type == 'click' && !Fade.keydown ){
					$('body').addClass('fade');
				}
			}
		}
		Fade._init();

		// this is a global component used to determine which breakpoint we're at
		// to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
		// callback accepts normal amount of params that a js event listener callback would have
		// but there's a custom property added to the event object called "name"
		// assuming in the callback the event object variable passed in is e: console.log(e.device);
		var Breakpoint = {
			name : '',
			_init : function(){
				$(window).on('resize load', Breakpoint._resizeLoadHander);
			},
			_resizeLoadHander : function(){
				if( $(window).width() > 1024 && Breakpoint.name != 'desktop' ){
					Breakpoint.name = 'desktop';
					Breakpoint._dispatchEvent();
				}
				else if( $(window).width() <= 1024 && $(window).width() > 640 && Breakpoint.name != 'tablet' ){
					Breakpoint.name = 'tablet';	
					Breakpoint._dispatchEvent();
				}
				else if( $(window).width() < 641 && Breakpoint.name != 'mobile' ){		
					Breakpoint.name = 'mobile';		
					Breakpoint._dispatchEvent();
				}
			},
			_dispatchEvent : function(){
				$(document).trigger($.Event('breakpoint', {device: Breakpoint.name}));
			}
		}
		Breakpoint._init();


		var HomeHero = {
			description : $('.homehero-arrow'),
			title : $('.homehero-title'),
			parallax : {
				background : $('.homehero-bgslider-item-tint'),
				_init : function(){
					$(window).on('scroll load', HomeHero.parallax._scrollLoadHandler);
				},
				_scrollLoadHandler : function(){
					if( HomeHero.parallax.background.length > 0 ){
						HomeHero.parallax.background.css('opacity', HomeHero.parallax._map(
							$(window).scrollTop(), 
							0, 
							HomeHero.parallax.background[0].clientHeight / 1.3, 
							0.5, 
							1
						));
					}
				},
				_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0},
			},
			arrowHandler : {
				arrow : $('.homehero-arrow, .homehero-titlecontainer'),
				_init : function(){
					HomeHero.arrowHandler.arrow.click(HomeHero.arrowHandler._arrowClickHandler);
				},
				_arrowClickHandler : function(e){
					e.preventDefault();
					$('body, html').animate({
						'scrollTop': HomeHero.parallax.background[0].clientHeight - Nav.nav[0].clientHeight,
					}, 500);		
				},
			},
			bgSlider : {
				container : $('.homehero-bgslider'),
				slides : $('.homehero-bgslider-item'),
				duration : 6000,
				fadeTime : 1000,
				_init : function(){
					if(HomeHero.bgSlider.slides.length > 1){
						HomeHero.bgSlider._nextSlide();	
					}
				},
				_nextSlide : function(){
					setTimeout(function(){
						$(HomeHero.bgSlider.slides[0]).fadeOut(HomeHero.bgSlider.fadeTime, function(){
							// move first slide to the end
							$(HomeHero.bgSlider.slides[0]).appendTo(HomeHero.bgSlider.container);
							// rebuild slides variable
							HomeHero.bgSlider.slides = $(HomeHero.bgSlider.slides.selector);
							// show the slide at the end
							$(HomeHero.bgSlider.slides[HomeHero.bgSlider.slides.length-1]).show();

							// recursion
							HomeHero.bgSlider._nextSlide();
						});
					}, HomeHero.bgSlider.duration);
				},
			},
			_init : function(){
				HomeHero.parallax._init();
				HomeHero.bgSlider._init();
				HomeHero.arrowHandler._init();
			}
		}

		HomeHero._init();

		var Grid = {
			grid : $('.grid'),
			items : $('.grid-item'),
			stopper : undefined,
			isotope : undefined,
			_init : function(){
				// $(window).on('resize load', Grid._resizeLoadHander);
			},
			_resizeLoadHander : function(e){
				if( e.type == 'load' ){
					if( Breakpoint.name == 'tablet' || Breakpoint.name == 'desktop' ){
						Grid.stopper = true;
					}
					else{
						Grid.stopper = false;
					}
				}

				if( Breakpoint.name == 'tablet' || Breakpoint.name == 'desktop' ){
					if( Grid.stopper == true ){
						Grid.stopper = false;
						// build isotope
						Grid.isotope = Grid.grid.isotope({
							itemSelector : Grid.items.selector,
							layoutMode : 'packery',
						});
					}
				}
				else{
					if( Grid.stopper == false ){
						Grid.stopper = true;
						// destroy isotope
						if(Grid.isotope != undefined){
							Grid.isotope.isotope('destroy');
						}
					}
				}
			},
		}
		Grid._init();

		/**
		 * [Nav handles the nav menu]
		 * @type {Object}
		 */
		var Nav = {
			/**
			 * [nav the nav container]
			 * @type {jquery obj}
			 */
			nav : $('.nav'),
			/**
			 * [navItems the items in the nav]
			 * @type {jquery obj}
			 */
			navItems : $('.nav-wrapper-items-left-menu-item'),
			/**
			 * [wrapper the nav wrapper]
			 * @type {jquery obj}
			 */
			wrapper : $('.nav-wrapper'),
			/**
			 * [mobile the mobile menu bar]
			 * @type {jquery obj}
			 */
			mobile : $('.nav-mobile'),
			/**
			 * [hamburger the mobile menu bar toggle]
			 * @type {jquery obj}
			 */
			hamburger : $('.nav-mobile-hamburger'),
			/**
			 * [searchToggle toggles the search form hide/show]
			 * @type {jquery obj}
			 */
			searchToggle : $('.nav-wrapper-items-right-searchicon, .searchform-closewrapper-close'),
			/**
			 * [searchform the search form container]
			 * @type {jquery obj}
			 */
			searchform : $('.searchform'),
			/**
			 * [isOpen stores the open state of the nav]
			 * @type {Boolean}
			 */
			isOpen : undefined,
			/**
			 * [inpageNavExists stores the state of the existence of the inpage nav]
			 * @type {Boolean}
			 */
			inpageNavExists : false,
			/**
			 * [backToTop the backtotop link if the inpage nav exists. otherwise is undefined]
			 * @type {jquery obj|undefined}
			 */
			backToTop : undefined,
			/**
			 * [_init entry point]
			 */
			_init : function(){
				
				// inpagenav exists?
				if( $('section.inpagenav').length > 0 ){
					/**
					 * [inpageNav the container of the inpage nav]
					 * @type {jquery obj}
					 */
					Nav.inpageNav = $('section.inpagenav:not(.dummy)');
					/**
					 * [inpageNavHeader the header of the inpage nav]
					 * @type {jquery obj}
					 */
					Nav.inpageNavHeader = $('section.inpagenav:not(.dummy) .inpagenav-headercontainer-header');
					// inpage nav does exist
					Nav.inpageNavExists = true;
					// if inpagenav had slideup applied to it too early
					if( Nav.inpageNav.css('display') == 'none' ){
						// show it briefly
						Nav.inpageNav.show();
					}
					// store the sticking point
					Nav.inpageNavStickyLocation = Nav.inpageNav.offset().top;
					// then remove it's style
					Nav.inpageNav.removeAttr('style');
					// is singlepost or primarysite's page?
					// because the sticking point will need to be at a different place
					if( Nav._isPrimarySinglePage() ){
						Nav.inpageNavStickyLocation = Nav.nav[0].clientHeight;	
						if( Nav._hasHero() ){
							Nav.inpageNav.css('top', Nav.nav[0].clientHeight);
						}
					}
					// set back to top link
					Nav.backToTop = $('.inpagenav-menu-item.backtotop');
					// listen for click events on the back to top link
					Nav.backToTop.on('click', Nav._backToTopClickHandler);
				}
				
				// listen for the scroll resize and load events
				$(window).on('scroll resize load', Nav._scrollResizeLoadHandler);
				// listen for click events on the hamburger
				Nav.hamburger.click(Nav._hamburgerClickHandler);
				// listen for click events on the search toggle
				Nav.searchToggle.click(Nav._searchClickHandler);
				// if someone presses esc key - fade out the search form
				$(document).keyup(function(e) {
					if(e.keyCode == 27){
						Nav.searchform.fadeOut();	
					}
				});
			},
			/**
			 * [_isPrimarySinglePage is singlepost or primarysite's page?]
			 * @return {Boolean}
			 */
			_isPrimarySinglePage : function(){
				return $('body.singlepost, body.primarypage').length > 0;
			},
			/**
			 * [_hasHero does the page have a hero?]
			 * @return {Boolean}
			 */
			_hasHero : function(){
				return $('.homehero').length > 0;
			},
			/**
			 * [_backToTopClickHandler handle the back to top animatino]
			 * @param  {obj} e the event object
			 */
			_backToTopClickHandler : function(e){
				// stop the link from doing it's normal thing
				e.preventDefault();
				// animate the window scrolling to the top of the screen
				$('body, html').animate({
					scrollTop : 0
				});
			},
			/**
			 * [_searchClickHandler toggle the search form fade]
			 * @param  {obj} e the event object
			 */
			_searchClickHandler : function(e){
				// did click on the search icon?
				if( $(e.target).hasClass('nav-wrapper-items-right-searchicon') ){
					Nav.searchform.fadeIn();	
				}
				else{
					Nav.searchform.fadeOut();
				}
			},
			/**
			 * [_hamburgerClickHandler handle the main nav open/close stuff]
			 * @return {obj} the event object
			 */
			_hamburgerClickHandler : function(){
				// are we above the nav?
				// this happens on the primary site page and sometimes the single post pages
				if($(window).scrollTop() < Nav.nav.offset().top){
					// if so then scroll us down to the nav
					$('body, html').animate({
						'scrollTop': $('.nav').offset().top,
					}, 250);
				}
				// is the hamburger closed?
				if(Nav.hamburger.hasClass('sficon-menu')){
					// open nav
					Nav._openNav();
				}
				else{
					// close nav
					Nav._closeNav();
				}
			},
			/**
			 * [_openNav opens the main nav]
			 */
			_openNav : function(){
				Nav.wrapper.addClass('nav-wrapper--visible');
				Nav.wrapper.fadeIn();
				Nav.hamburger.removeClass('sficon-menu');
				Nav.hamburger.addClass('sficon-close');
				Nav.isOpen = true;
			},
			/**
			 * [_closeNav closes the nav]
			 */
			_closeNav : function(){
				Nav.wrapper.removeClass('nav-wrapper--visible');
				Nav.wrapper.fadeOut('400', function() {
					Nav.wrapper.removeAttr('style');
				});;
				Nav.hamburger.addClass('sficon-menu');
				Nav.hamburger.removeClass('sficon-close');
				Nav.isOpen = false;
			},
			/**
			 * [_scrollResizeLoadHandler handles the bulk of the navs logic when scrolling/loading/resizing]
			 * @param  {obj} e the event object
			 */
			_scrollResizeLoadHandler : function(e){
				// if resizing
				if(e.type == 'resize'){
					// close the nav if we're on the desktop view and if it's already open
					if( $(window).width() > 1024 && Nav.isOpen ){
						Nav._closeNav();
					}
				}
				if( (e.type == 'resize' || e.type == 'load') && Nav.inpageNavExists ){	
					if( $(window).width() < 641 && Nav._isPrimarySinglePage() ){
						Nav.inpageNav.css('top', 0);
					}
					else{
						// Nav.inpageNav.css('top', Nav.nav[0].clientHeight);
					}
				}
				// if hero exists
				if( $('.homehero').length > 0 ){
					// if we're below the hero - nav height
					if( $(window).scrollTop() >= (HomeHero.description.length > 0 ? HomeHero.description.offset().top : HomeHero.title[0].clientHeight) - Nav.nav[0].clientHeight ){
						Nav.nav.addClass('nav--belowhero');
					}
					else{
						Nav.nav.removeClass('nav--belowhero');
					}
				}
				else{
					Nav.nav.addClass('nav--belowhero');
				}
				// if inpagenav exists
				if( Nav.inpageNavExists && typeof Nav.inpageNavStickyLocation !== 'undefined' ){
					// are we below the location where the inpage nav shows up?
					if( $(window).scrollTop() >= Nav.inpageNavStickyLocation ){
						Nav.inpageNavHeader.css('opacity', 1);
						Nav.nav.slideUp('fast');
						Nav.inpageNav.addClass('inpagenav--active');
					}
					else{
						Nav.inpageNavHeader.css('opacity', 0);
						Nav.nav.slideDown('fast', function(){
							Nav.inpageNav.removeClass('inpagenav--active');
						});
					}
				}
			},
		}

		Nav._init();


		var Sliders = {
			_init : function(){
				baguetteBox.run('.slider');
			}
		}
		Sliders._init();

		var Calendar = {
			widget : $('.calendar-widgetcontainer-widget'),
			loadingIcon : $('.calendar-grid-loading'),
			grid : $('.calendar-grid'),
			cells : undefined,
			monthNames : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			availableDates : [],
			events : undefined,
			responseItems : [],
			url : undefined,

			_init : function(){
				// if there's a calendar to populate with events
				if( $('body').hasClass('primarysite') && $('body').hasClass('events') ){
					// set the url
					Calendar.url = 'https://www.googleapis.com/calendar/v3/calendars/' + GoogleCal.id + '/events?key=' + GoogleCal.key;
					// kick off the calendar events stuff
					Calendar._getCalendarEvents({}, 'success');
				}
			},
			_getCalendarEvents : function(response, status){
				// if GET request to google cal api was successful
				if( status == 'success' ){
					// timeout to avoid spamming server
					setTimeout(function(response, status){
						// add items to Calendar.responseItems
						if( typeof response.items !== 'undefined' ){
							response.items.forEach(function(item){
								Calendar.responseItems.push(item);
							});
						}
						// nextSyncToken being present means there's no more results to get
						// it not being there means nextPageToken is there which would load more results
						var pt = '';
						if( typeof response.nextSyncToken === 'undefined' ){
							// save pageToken to get the rest of the results
							if( typeof response.nextPageToken !== 'undefined' ){
								pt = response.nextPageToken;
							}
						}
						// we're done
						else{
							Calendar._responseHandler();
							return null;
						}
						// init GET url
						var url = pt.length > 0 ? Calendar.url + '&pageToken=' + pt : Calendar.url;
						// send ajax request
						$.ajax({
							url: url,
							dataType: 'json',
						})
						.done(Calendar._getCalendarEvents)
						.fail(function(error) {
							console.log("error: ", error);
						});
					}, 50, response, status, Calendar);
				}
			},
			_responseHandler : function(){
				Calendar.loadingIcon.slideUp();
				Calendar.loadingIcon.fadeOut();

				Calendar._sortFilterResults();

				Calendar.widget.datepicker({
					nextText: "",
					prevText: "",
					dateFormat: "yy-mm-dd",
					onSelect : Calendar._onCellClickHandler,
					beforeShowDay : Calendar._beforeShowDayHandler,
				});

			},			
			_onCellClickHandler : function(d){
				$('html, body').animate({
					scrollTop : $('.calendar-grid-item.' + d).offset().top - (Nav.nav.height() + 20)
				})
			},
			_beforeShowDayHandler : function(date){
				// if d is in availableDates
				var theDate = $.datepicker.formatDate("yy-mm-dd", date);
				if( $.inArray(theDate, Calendar.availableDates) != -1 ){
					return [true, 'clickable'];
				}
				else{
					return false;
				}
			},
			_sortFilterResults : function(){
				// filter out cancelled events
				var itemCount = 1;
				var filteredItems = Calendar.responseItems.filter(function(item) {
					if( item.status !== 'cancelled' && Date.now() < new Date(item.start[Object.keys(item.start)[0]]).getTime() ){
						return item;
					}
				});
				// sort date : newest to oldest
				Calendar.events = filteredItems.sort(function(a, b){
					return new Date(a.start.dateTime) - new Date(b.start.dateTime);
				});

				Calendar._buildAvailableDates(Calendar.events);
			},
			_buildAvailableDates : function(items){
				// populates Calendar.availableDates with date objects
				items.forEach(function(item){
					var startDate = new Date(item.start[Object.keys(item.start)[0]]);
					var endDate = new Date(item.end[Object.keys(item.end)[0]]);

					Calendar.availableDates.push(startDate);

					// if there's an event that takes place over multiple days
					// get the days between the start and end dates
					// including the start & end date
					if( startDate.getDate() != endDate.getDate() ){
						var numDays = Math.round( (endDate - startDate) / (1000 * 60 * 60 * 24) );
						var inc = 0;
						while(inc < numDays){
							var newDate = new Date(startDate);
							newDate.setDate(startDate.getDate() + inc + 1);
							Calendar.availableDates.push(newDate);
							inc++;
						}
					}
				});
				// formats available date to a more comparable format for beforeShowDay
				Calendar.availableDates.forEach(function(val, index){
					Calendar.availableDates[index] = $.datepicker.formatDate('yy-mm-dd', val);
				});
				Calendar._renderEvents();
			},
			
			_renderEvents : function(){
				if(Calendar.events.length > 0){
					Calendar.events.forEach(function(item){
						var description = item.description == undefined ? '' : '<div class="calendar-grid-item-description section-grid-item-content">' + item.description + '</div>';
						Calendar.grid.append(
							'<div class="calendar-grid-item section-grid-item ' + $.datepicker.formatDate( 'yy-mm-dd', new Date(item.start[Object.keys(item.start)[0]]) ) + '">' +
								'<h2 class="calendar-grid-item-header section-grid-item-header">' + item.summary + '</h2>' +
								'<h3 class="calendar-grid-item-subheader section-grid-item-subheader">' + Calendar._getFormatedDate( item ) + '</h3>' + 
								description +
							'</div>'
						);
					});
				}
				else{
					Calendar.grid.append(
						'<div class="calendar-grid-item section-grid-item">' +
							'<h2 class="calendar-grid-item-header section-grid-item-header" style="text-align: center;">Sorry, no upcoming events.</h2>' +
						'</div>'
					);
				}
			},
			_getFormatedDate : function(item){
				var start = new Date( item.start[Object.keys(item.start)[0]] );
				var end = new Date( item.end[Object.keys(item.end)[0]] );

				var ampm = ' AM';
				var hours = '';
				var minutes = '';

				if( start.getHours() > 12){
					ampm = ' PM';
					hours = (start.getHours() - 12).toString();
				}
				
				if( start.getHours() == 0 || start.getHours == 12 ){
					hours = '12';
				}
				else if ( start.getHours() < 12 ){
					hours = (start.getHours()).toString();
				}

				if( start.getMinutes() == 0 ){
					minutes = '00';
				}
				else{
					minutes = (start.getMinutes()).toString();
				}
				// if event ends on a different day than the day it started on
				if( start.getDate() != end.getDate() ){
					return Calendar.monthNames[start.getMonth()] + ' ' + start.getDate() + '-' + end.getDate() + ' ' + start.getFullYear();
				}
				else{
					return Calendar.monthNames[start.getMonth()] + ' ' + start.getDate() + ' ' + start.getFullYear() + ', ' + hours + ':' + minutes + ampm;
				}
			},
		};

		Calendar._init();



		var Search = {
			form : $('.searchform'),
			checkbox : $('.searchform-wrapper-checkbox'),
			_init : function(){
				$(window).on('load', Search._updateAction);
				Search.checkbox.on('change', Search._updateAction);
			},
			_updateAction : function(){
				if( Search.checkbox.is(':checked') ){
					Search.form.attr('action', ProjectUrls.current);
				}
				else{
					Search.form.attr('action', ProjectUrls.main);	
				}
			},
		}

		Search._init();


		var TheStacks = {
			carousel : {
				container : $('.thestacksgrid-main-grid.featured'),
				dotsContainer : $('.thestacksgrid-main-grid-dots'),
				dots : $('.thestacksgrid-main-grid-dots-dot'),
				slides : $('.thestacksgrid-main-grid.featured .thestacksgrid-main-grid-post'),
				currentSlide : 0,
				autoRotateSpeed : 4000,
				autoRotate : true,
				_init : function(){
					if( TheStacks.carousel.slides.length > 0 ){
						$(document).on('breakpoint', TheStacks.carousel._breakpointHandler);
					}
				},
				_breakpointHandler : function(e){
					if( e.device == 'desktop' ){
						TheStacks.carousel.build._init();					
					}
					else{
						TheStacks.carousel.destroy._init();
					}
				},
				build : {
					_init : function(){
						TheStacks.carousel.currentSlide = 0;
						TheStacks.carousel.autoRotate = true;

						TheStacks.carousel.build._setContainerHeight();
						TheStacks.carousel.build._setDotsTop();
						TheStacks.carousel.build._createDots();

						if( TheStacks.carousel.slides.length > 1 ){
							TheStacks.carousel.build._autoRotate();
						}
						
						TheStacks.carousel.dotsContainer.on('click', '.thestacksgrid-main-grid-dots-dot', TheStacks.carousel.build._dotClickHandler);
					},
					_initCarousel : function(){
						// show only the first slide
						TheStacks.carousel.slides.hide();
						$(TheStacks.carousel.slides[TheStacks.carousel.currentSlide]).show();

						// repopulate dots object
						TheStacks.carousel.dots = $(TheStacks.carousel.dots.selector);

						// show only the first dot
						TheStacks.carousel.dots.removeClass('active');
						$(TheStacks.carousel.dots[TheStacks.carousel.currentSlide]).addClass('active');
					},
					_getTallestSlide : function(){
						var tallest = $(TheStacks.carousel.slides[0]);
						$.each(TheStacks.carousel.slides, function(index, el){
							if( tallest.height() <= $(el).height() ){
								tallest = $(el);
							}
						});
						return tallest;
					},
					_setContainerHeight : function(){
						TheStacks.carousel.container.height( TheStacks.carousel.build._getTallestSlide().height() );
					},
					_setDotsTop : function(){
						TheStacks.carousel.dotsContainer.css('top', TheStacks.carousel.build._getTallestSlide().height() + 40 );
					},
					_createDots : function(){
						$.each(TheStacks.carousel.slides, function(index, el){
							TheStacks.carousel.dotsContainer.append('<div class="thestacksgrid-main-grid-dots-dot"></div>');
						});
						TheStacks.carousel.build._initCarousel();
					},
					_dotClickHandler : function(e){
						var index = 0;
					    while( (e.target = e.target.previousSibling) != null ) ++index;
					    TheStacks.carousel.build._goToSlide(index);

					},
					_goToSlide : function(num){

						TheStacks.carousel.currentSlide = num;

						// update slide
						TheStacks.carousel.slides.fadeOut();
						$(TheStacks.carousel.slides[num]).fadeIn();

						// update dots
						TheStacks.carousel.dots.removeClass('active');
						$(TheStacks.carousel.dots[num]).addClass('active');
					},
					_autoRotate : function(){
						if( TheStacks.carousel.autoRotate ){
							setTimeout(function(){
								if( TheStacks.carousel.autoRotate ){
									var s = TheStacks.carousel.currentSlide + 1 > TheStacks.carousel.slides.length - 1 ? 0 : TheStacks.carousel.currentSlide + 1;
									TheStacks.carousel.build._goToSlide(s)
									TheStacks.carousel.build._autoRotate();
								}
							}, TheStacks.carousel.autoRotateSpeed);
						}
					},
				},
				destroy : {
					_init : function(){
						TheStacks.carousel.autoRotate = false;
						TheStacks.carousel.slides.show();
						TheStacks.carousel.container.css('height', '');
					},
				}
			},
			posts : {
				grid : $('.thestacksgrid-main-grid:not(.featured)'),
				_init : function(){
					$(document).on('breakpoint', TheStacks.posts._breakpointHandler);
				},
				_breakpointHandler : function(e){
					if( e.device == 'desktop' ){
						TheStacks.posts.grid.isotope({
							itemSelector : '.thestacksgrid-main-grid:not(.featured) .thestacksgrid-main-grid-post',
						});
					}
					else{
						TheStacks.posts.grid.isotope().isotope('destroy');
					}
				}
			},
			_init : function(){
				TheStacks.carousel._init();
				TheStacks.posts._init();
			},
		};

		TheStacks._init();



		var ShareHandler = {
			links: $('.bloggrid-item-postcontainer-post-social-link.share, .thestacksgrid-main-grid-post-textcontainer-share-link.share, .bloggrid-item-share-link.share'),
			_init : function(){
				ShareHandler.links.on('click', ShareHandler._clickHandler);
			},
			_clickHandler : function(e){
				e.preventDefault();
				ShareHandler._copyToClipboard(e.target.dataset['url']);

				ShareHandler._showMessage();

			},
			_copyToClipboard : function(text) {
			    if (window.clipboardData && window.clipboardData.setData) {
			        // IE specific code path to prevent textarea being shown while dialog is visible.
			        return clipboardData.setData("Text", text); 

			    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
			        var textarea = document.createElement("textarea");
			        textarea.textContent = text;
			        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
			        document.body.appendChild(textarea);
			        textarea.select();
			        try {
			            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
			        } catch (ex) {
			            console.warn("Copy to clipboard failed.", ex);
			            return false;
			        } finally {
			            document.body.removeChild(textarea);
			        }
			    }
			},
			_showMessage : function(){
				$('body').append('<div class="copy-message">' +
									 '<div class="copy-message-text">' +
									     '<i class="sficon sficon-info"></i> The link has been copied to your clipboard.' +
									 '</div>' +
								 '</div>');
				$('.copy-message').fadeOut(2500, function(){
					$('.copy-message').remove();
				});
			}
		};

		ShareHandler._init();



		var Contact = {
			form : $('.contact-wrapper-form'),
			email : $('.contact-wrapper-form-email'),
			messages : $('.contact-wrapper-messages'),
			_init : function(){
				if( Contact.form.length > 0 ){
					Contact.form.on('submit', Contact._submitHandler);
				}
			},
			_submitHandler : function(e){
				e.preventDefault();
				$.post(AJAXURL, {
					action : 'handle_email_submit',
					email : Contact.email.val()
				}, Contact._successHandler);
			},
			_successHandler : function(response){
				// is an error
				try{
					var response = JSON.parse(response);
					console.log(response.error);
					Contact.messages.addClass('error');
					Contact.messages.html(response.error);
				}
				// is valid
				catch(e){
					Contact.form.hide();
					Contact.messages.removeClass('error');
					Contact.messages.addClass('success');
					Contact.messages.html(response);
				}
				// clear email form
				Contact.email.val('');
			}
		}

		Contact._init();


		var PCTester = {
			_init : function(){
				if( window.navigator.platform.toLowerCase().indexOf('win') >= 0 ){
					$('body').addClass('win');
				}
			}
		};

		PCTester._init();


		var Seemore = {
			grid : $('.bloggrid'),
			items : $('.bloggrid-item'),
			button : $('.seemore-button'),
			loader : $('.seemore-loader'),
			_init : function(){
				// listen for click event on seemore button
				Seemore.button.on('click', Seemore._clickHandler);
			},
			_clickHandler : function(){
				// show loader hide button
				Seemore.button.animate({ height: 'toggle', opacity: 'toggle' }, 'fast');
				Seemore.loader.animate({ height: 'toggle', opacity: 'toggle' }, 'fast');
				// build data
				var data = {
					action : 'seemore',
					offset : Seemore.items.length
				};
				// setup term_id in case we're in the archive page or search page
				if( typeof TERM_ID !== 'undefined' ){
					data['term_id'] = TERM_ID;
				}
				if( typeof TAXONOMY !== 'undefined' ){
					data['taxonomy'] = TAXONOMY;
				}
				// query wpdb
				$.post(AJAXURL, data, function(response){
					// cast response to json
					var response = JSON.parse(response);
					// add items to grid
					Seemore.grid.append(response.data);
					// rebuild items var
					Seemore.items = $('.bloggrid-item');
					// show button hide loader if there's more posts otherwise hide everything
					if( Seemore.items.length < response.totalposts ){
						Seemore.button.animate({ height: 'toggle', opacity: 'toggle' }, 'fast');
						Seemore.loader.animate({ height: 'toggle', opacity: 'toggle' }, 'fast');
					}
					else{
						Seemore.button.fadeOut('fast');
						Seemore.loader.fadeOut('fast');
					}
				});
			}
		};

		Seemore._init();

		var NewsletterModal = {
			join : $('.footer-creditjoin-join-button'),
			close : $('.footer-creditjoin-join-signup-wrapper-close'),
			modal : $('.footer-creditjoin-join-signup'),
			_init : function(){
				NewsletterModal.join.on('click', NewsletterModal._joinClickHandler);
				NewsletterModal.close.on('click', NewsletterModal._closeClickHandler);
			},
			_joinClickHandler : function(e){
				NewsletterModal.modal.fadeIn();
			},
			_closeClickHandler : function(e){
				NewsletterModal.modal.fadeOut();
			}

		};

		NewsletterModal._init();


		var EMSeemore = {
			grid : $('.events-grid'),
			button : $('.events-seemore'),
			_init : function(){
				if( typeof EMPOSTS != 'undefined' ){
					// remove seemore if there's none to pull on load
					if( $('.events-grid-item').length >= EMPOSTS ){
						EMSeemore.button.remove();
					}
					else{
						EMSeemore.button.on('click', EMSeemore._clickHandler);
					}
				}
			},
			_clickHandler : function(e){
				var offset = $('.events-grid-item').length;

				var data = {
					action : 'emseemore',
					offset : offset,
					archived : window.location.search.indexOf('archived') !== -1 ? true : false,
				};

				$.post(ajaxurl, data, EMSeemore._responseHandler);
			},
			_responseHandler : function(response){
				EMSeemore.grid.append(response);
				if( $('.events-grid-item').length >= EMPOSTS ){
					EMSeemore.button.remove();
				}
			}
		};
		// commented this out because there's no seemore for events needed anymore
		// EMSeemore._init();



	});

})( jQuery, window, document );

