<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php switch_to_blog(get_current_blog_id()); ?>
		<title><?php echo get_bloginfo('name') . ' | ' . wp_trim_words( get_bloginfo('description'), 10, '' );?></title>
		<?php include(locate_template('opengraph.php')); ?>
		<?php wp_head(); ?>		
		<?php include(locate_template('color-picker.php')); ?>
		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	</head>
	<?php
	$class_list = [' '];
	if(get_current_blog_id() == 1){
		array_push($class_list, 'primarysite');	
		if( is_page() ){
			array_push($class_list, 'primarypage');	
		}
	}
	else{
		array_push($class_list, 'projectsite');		
	}
	if(is_single()){
		array_push($class_list, 'singlepost');
	}
	if(get_field('theme-color-tint-toggle', 'option')){
		array_push($class_list, 'hastint');
	}
	$class_list = array_merge($class_list, get_body_class());
	?>
	<body class="fade<?php echo implode(' ', $class_list) ?>">