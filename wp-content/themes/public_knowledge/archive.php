<?php get_header(); ?>
<div class="mainBackgroundColor">
	<?php
		include(locate_template( 'modules/nav.php' ));

		$term = get_queried_object();
		$searched_for = '';
		if( is_tag() ){
			$searched_for = 'tag';
		}
		if( is_category() ){
			$searched_for = 'category';
		}

		$args = array(
			'posts_per_page' => 10,
			'tax_query' => array(
				array(
					'taxonomy' => $term->taxonomy,
					'field' => 'id',
					'terms' => $term->term_id,
				),
			),
		);
		?>
		<script type="text/javascript">
			var TERM_ID = <?php echo $term->term_id; ?>;
			var TAXONOMY = '<?php echo $term->taxonomy; ?>';
		</script>
		<?php
		$the_query = new WP_Query($args);
		?>
		<section class="archive section">
			<h1 class="archive-header section-header"><?php echo $the_query->found_posts; ?> posts with the <?php echo $searched_for; ?> "<?php echo $term->name;?>"</h1>
			<?php
			if( $the_query->have_posts() ):
				include(locate_template( 'modules/bloggrid.php' ));
			endif;
			?>
			<?php
			if( $the_query->found_posts > 10 ){
				?>	
					<div class="seemore">
						<img src="<?php echo admin_url( 'images/spinner-2x.gif' ); ?>" class="seemore-loader">
						<div class="seemore-button">See more</div>
					</div>
				<?php
			}
			?>
		</section>
		<?php
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>