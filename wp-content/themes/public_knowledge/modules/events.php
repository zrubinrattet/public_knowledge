<section class="events section wysiwyg">
	<div class="events-sidebar wysiwyg-sidebar">
		<?php 
			echo do_shortcode( '[events_calendar full="0" long_events="1"]' ); 
			if( isset($_GET['archived']) ){
				?>
					<a href="<?php echo network_site_url( 'events' ); ?>" class="events-sidebar-link">Current Events</a>
				<?php
			}
			else{
				?>
					<a href="<?php echo network_site_url( 'events' ) . '?archived' ?>" class="events-sidebar-link">Past Events</a>
				<?php
			}
		?>
	</div>
	<div class="events-grid wysiwyg-content">
		<?php 
			$emutil = new EMUtil();

			$args = isset($_GET['archived']) ? array('archived' => true) : array();

			$args['cat'] = isset($cat) ? $cat : '';

			$events = $emutil->get_events($args);

			if( !empty($events) ):
				foreach( $events as $event ) :
					echo $emutil->get_event_html($event);
				endforeach;
			endif;

			// var_dump(count($events));
			// error_log(count($events) >= (isset($args['limit']) ? $args['limit'] : 5) ? 'true' : 'false');
		?>
	</div>
	<?php if( count($events) >= (isset($args['limit']) ? $args['limit'] : 5) ): ?>
		<!-- <div class="events-seemore">See more</div> -->
	<?php endif; ?>
</section>