<section class="projects section">
	<div class="grid">
		<?php 
			$bg_image = get_template_directory_uri() . '/library/img/logo-ghost.png';
			if( !empty( get_field('stacks_bg_image', 'option') ) ){
				$bg_image = get_field('stacks_bg_image', 'option');
			}
		?>
		<?php 
			$rows = get_field('site_sort', 'option');
			foreach($rows as $row):
			
				$site;
				$hex;

				if( $row['site_name'] !== 'The Stacks' ){
					$site = get_site_by_path($_SERVER['HTTP_HOST'], '/' . strtolower( str_replace(' ', '-', $row['site_name']) ) . '/' );
					if($site->blog_id == '1' || $row['in_project_grid'] == false) continue;
					
					switch_to_blog($site->blog_id);

					$hex = get_field('theme-color-tint', 'option');
				}
				else{
					$site = get_page_by_path( strtolower( str_replace(' ', '-', $row['site_name']) ) );

					switch_to_blog(1);

					$hex = '#FFFFFF';
				}

				$grid_item_url = '';
				if( get_class($site) == 'WP_Post' ){
					$grid_item_url = get_permalink( $site->ID );
				}
				elseif( get_class($site) == 'WP_Site' ){
					$grid_item_url = site_url();
				}

				if( !empty(get_field('hero_bg_image', 'option')) ):
		?>			
					<a href="<?php echo $grid_item_url; ?>" class="grid-item" style="background-image: url('<?php echo get_field('hero_bg_image', 'option'); ?>');">
				<?php else: ?>
					<a href="<?php echo $grid_item_url; ?>" class="grid-item noimage" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/img/logo-ghost.png');">
				<?php endif; ?>
			<div class="grid-item-caption">
				<h3 class="grid-item-caption-sitename"<?php echo !is_null($hex) ? ' style="color: rgba(' . implode(', ', ColorBeast::hex_to_rgb($hex)) . ', 1);"' : ''; ?>>
					<?php 
						if( !empty($row['site_name_alt']) ){
							echo $row['site_name_alt']; 
						}
						else{
							echo $row['site_name']; 
						}
					?>	
				</h3>
			</div>
			<div class="grid-item-description"<?php 
				if( !is_null($hex) && $row['site_name'] !== 'The Stacks' ) {
					echo ' style="background-color: rgba(' . implode(', ', ColorBeast::hex_to_rgb($hex)) . ', 1);"'; 
				}
				elseif( !is_null($hex) && $row['site_name'] == 'The Stacks' ){
					echo ' style="background-color: rgba(0, 0, 0, 1); color: #fff;"'; 
				}
				?>>
				<?php 
					if( $row['site_name'] !== 'The Stacks' ){
						echo wp_trim_words( get_bloginfo('description'), 22, '...'); 
					}
					else{
						echo wp_trim_words( $site->post_content, 22, '...'); 
					}
				?>
			</div>
			<div class="grid-item-tint"></div>
		</a>

			<?php 

			restore_current_blog();
			endforeach; ?>
	</div>
</section>