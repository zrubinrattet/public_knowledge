<?php 
global $post;
?>

<section class="calendar section">
	<div class="calendar-widgetcontainer">
		<div class="calendar-widgetcontainer-widget"></div>
	</div>
	<div class="calendar-grid section-grid">
		<div class="calendar-grid-loading">
			<i class="calendar-grid-loading-icon sficon sficon-refresh"></i>
		</div>
	</div>
</section>