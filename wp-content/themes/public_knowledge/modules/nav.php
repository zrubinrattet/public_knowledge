<?php
	if( !empty(get_field('disable_hero') )){
		$hashero = true;
	}
?>
<nav class="nav<?php echo isset($has_hero) && $has_hero ? ' nav--hashero' : ''; ?>">
	<div class="nav-wrapper">
		<div class="nav-wrapper-items">
			<div class="nav-wrapper-items-left">
				<a class="nav-wrapper-items-left-link" href="<?php echo is_main_site( get_current_blog_id() ) ? site_url() : network_site_url() ; ?>">
					<svg version="1.1" class="nav-wrapper-items-left-link-logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px">
						<path class="nav-wrapper-items-left-link-logo-path" d="M2.1,9.7v5.9H0.4V0.8h4.9c3.1,0,4.9,1.6,4.9,4.4S8.3,9.6,5.3,9.6L2.1,9.7L2.1,9.7z M2.1,2.5V8h3.1
							c2,0,3.1-0.9,3.1-2.8S7.2,2.4,5.2,2.4H2.1V2.5z M12.3,11.8c0,2.4,1.6,4,4,4c1.2,0,2.5-0.5,3.3-1.8v1.6h1.7V4.7h-1.7v6.6
							c0,1.8-1.2,2.9-2.8,2.9c-1.7,0-2.7-1-2.7-2.8V4.7h-1.7L12.3,11.8 M34.8,10.1c0,3.4-2,5.7-4.7,5.7c-1.6,0-2.8-0.8-3.5-1.9v1.7h-1.7
							V0.2h1.7v6.2c0.7-1.2,1.9-1.9,3.5-1.9C32.8,4.5,34.8,6.7,34.8,10.1 M26.5,10.1c0,2.4,1.3,4,3.3,4s3.3-1.7,3.3-4c0-2.4-1.3-4-3.3-4
							C27.8,6.1,26.5,7.7,26.5,10.1 M39.4,0.2h-1.7v15.4h1.7V0.2z M45,1.2c0,0.7-0.5,1.2-1.2,1.2s-1.2-0.5-1.2-1.2S43.1,0,43.8,0
							S45,0.5,45,1.2 M44.7,15.6H43V4.7h1.7V15.6z M56.1,6c-0.8-1-2.1-1.6-3.6-1.6c-3.1,0-5.1,2.3-5.1,5.7s2,5.7,5.1,5.7
							c1.5,0,2.8-0.6,3.6-1.6l-1-1.2c-0.7,0.7-1.6,1.1-2.6,1.1c-1.9,0-3.3-1.5-3.3-4s1.4-4,3.3-4c1,0,1.9,0.4,2.6,1.1L56.1,6 M8.1,35.4
							h3.5l-5.9-7.5l5.6-7.3H7.8l-5,6.8v-6.8H0v14.7h2.8v-6.8L8.1,35.4z M22.6,28c0-2.4-1.6-3.9-3.8-3.9c-1.1,0-2.2,0.4-2.9,1.4v-1.1h-2.6
							v11.1H16v-6.7c0-1.4,0.9-2.1,2.1-2.1s1.9,0.8,1.9,2.2v6.6h2.7V28 M35.3,29.9c0,3.5-2.2,5.8-5.3,5.8s-5.3-2.3-5.3-5.8
							s2.2-5.8,5.3-5.8S35.3,26.4,35.3,29.9 M27.5,29.9c0,2,1,3.3,2.5,3.3s2.5-1.3,2.5-3.3s-1.1-3.3-2.5-3.3
							C28.5,26.6,27.5,27.9,27.5,29.9 M46,24.3h-2.8L41,32.2l-2.1-7.9h-2.8l3.3,11.1h3.1l2.1-7.6l2.2,7.6h3.1l3.3-11.1h-2.8l-2.1,7.9
							L46,24.3z M57.5,20h-2.7v15.4h2.6L57.5,20z M69.6,29.9c0,0.3,0,0.7,0,0.9h-7c0.2,1.8,1.2,2.6,2.6,2.6c0.9,0,1.8-0.3,2.5-1l1.6,1.8
							c-1.1,1.1-2.6,1.5-4.2,1.5c-3.1,0-5.2-2.2-5.2-5.8s2-5.9,5-5.9C67.7,24,69.6,26.3,69.6,29.9 M62.6,28.8h4.3
							c-0.2-1.5-0.9-2.4-2.1-2.4C63.5,26.4,62.8,27.3,62.6,28.8 M78.8,25.3V20h2.7v15.4h-2.6v-1.1c-0.7,0.8-1.6,1.3-3,1.3
							c-2.7,0-4.7-2.4-4.7-5.8s2-5.8,4.7-5.8C77.2,24,78.1,24.5,78.8,25.3 M74,29.9c0,1.9,1,3.3,2.5,3.3s2.5-1.3,2.5-3.3s-1-3.3-2.5-3.3
							C74.9,26.6,74,27.9,74,29.9 M91.3,25.4v-1H94v10.4c0,3.3-2.1,5.3-5.3,5.3c-1.7,0-3.3-0.5-4.3-1.4l1.3-2c0.9,0.7,1.8,1,2.9,1
							c1.6,0,2.7-0.9,2.7-2.7v-1c-0.7,0.8-1.6,1.3-2.9,1.3c-2.7,0-4.7-2.2-4.7-5.6s1.9-5.6,4.7-5.6C89.7,24,90.7,24.6,91.3,25.4
							 M86.6,29.7c0,1.8,0.9,3.1,2.4,3.1s2.4-1.2,2.4-3.1s-1-3.1-2.4-3.1C87.5,26.6,86.6,27.9,86.6,29.7 M105.9,29.9c0,0.3,0,0.7,0,0.9h-7
							c0.2,1.8,1.2,2.6,2.6,2.6c0.9,0,1.8-0.3,2.5-1l1.6,1.8c-1.1,1.1-2.6,1.5-4.2,1.5c-3.1,0-5.2-2.2-5.2-5.8s2-5.9,5-5.9
							C103.9,24,105.9,26.3,105.9,29.9 M98.9,28.8h4.3c-0.2-1.5-0.9-2.4-2.1-2.4C99.8,26.4,99.1,27.3,98.9,28.8"/>
						</svg>
					</svg>
				</a>
				<a class="nav-wrapper-items-left-link--mobile" href="<?php echo site_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/library/img/logo-black.png">
				</a>
			</div>
			<div class="nav-wrapper-items-right">
				<div class="nav-wrapper-items-right-menu">
					<a href="<?php echo network_site_url('pk-library') ?>" class="nav-wrapper-items-right-menu-item">Library</a>	
					<a href="<?php echo network_site_url('projects') ?>" class="nav-wrapper-items-right-menu-item">Projects</a>	
					<a href="<?php echo network_site_url('events') ?>" class="nav-wrapper-items-right-menu-item">Calendar</a>	
					<a href="<?php echo network_site_url('the-stacks') ?>" class="nav-wrapper-items-right-menu-item">The Stacks</a>
					<a href="<?php echo network_site_url('about') ?>" class="nav-wrapper-items-right-menu-item">About</a>
				</div>
				<div class="nav-wrapper-items-right-social">
					<a href="<?php the_field('facebook_url', 'option') ?>" class="nav-wrapper-items-right-social-link">
						<i class="nav-wrapper-items-right-social-link-icon sficon sficon-facebook"></i>
					</a>
					<a href="<?php the_field('twitter_url', 'option') ?>" class="nav-wrapper-items-right-social-link">
						<i class="nav-wrapper-items-right-social-link-icon sficon sficon-twitter"></i>
					</a>
					<a href="<?php the_field('instagram_url', 'option') ?>" class="nav-wrapper-items-right-social-link">
						<i class="nav-wrapper-items-right-social-link-icon sficon sficon-instagram"></i>
					</a>
				</div>
				<i class="nav-wrapper-items-right-searchicon sficon sficon-search"></i>
			</div>		
		</div>
		<div class="nav-wrapper-tint"></div>
	</div>
	<div class="nav-mobile">
		<a href="<?php echo network_site_url() ?>" class="nav-mobile-home">
			<svg width="106" height="40" version="1.1" class="nav-mobile-home-logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px">
				<path class="nav-wrapper-items-left-link-logo-path" d="M2.1,9.7v5.9H0.4V0.8h4.9c3.1,0,4.9,1.6,4.9,4.4S8.3,9.6,5.3,9.6L2.1,9.7L2.1,9.7z M2.1,2.5V8h3.1
					c2,0,3.1-0.9,3.1-2.8S7.2,2.4,5.2,2.4H2.1V2.5z M12.3,11.8c0,2.4,1.6,4,4,4c1.2,0,2.5-0.5,3.3-1.8v1.6h1.7V4.7h-1.7v6.6
					c0,1.8-1.2,2.9-2.8,2.9c-1.7,0-2.7-1-2.7-2.8V4.7h-1.7L12.3,11.8 M34.8,10.1c0,3.4-2,5.7-4.7,5.7c-1.6,0-2.8-0.8-3.5-1.9v1.7h-1.7
					V0.2h1.7v6.2c0.7-1.2,1.9-1.9,3.5-1.9C32.8,4.5,34.8,6.7,34.8,10.1 M26.5,10.1c0,2.4,1.3,4,3.3,4s3.3-1.7,3.3-4c0-2.4-1.3-4-3.3-4
					C27.8,6.1,26.5,7.7,26.5,10.1 M39.4,0.2h-1.7v15.4h1.7V0.2z M45,1.2c0,0.7-0.5,1.2-1.2,1.2s-1.2-0.5-1.2-1.2S43.1,0,43.8,0
					S45,0.5,45,1.2 M44.7,15.6H43V4.7h1.7V15.6z M56.1,6c-0.8-1-2.1-1.6-3.6-1.6c-3.1,0-5.1,2.3-5.1,5.7s2,5.7,5.1,5.7
					c1.5,0,2.8-0.6,3.6-1.6l-1-1.2c-0.7,0.7-1.6,1.1-2.6,1.1c-1.9,0-3.3-1.5-3.3-4s1.4-4,3.3-4c1,0,1.9,0.4,2.6,1.1L56.1,6 M8.1,35.4
					h3.5l-5.9-7.5l5.6-7.3H7.8l-5,6.8v-6.8H0v14.7h2.8v-6.8L8.1,35.4z M22.6,28c0-2.4-1.6-3.9-3.8-3.9c-1.1,0-2.2,0.4-2.9,1.4v-1.1h-2.6
					v11.1H16v-6.7c0-1.4,0.9-2.1,2.1-2.1s1.9,0.8,1.9,2.2v6.6h2.7V28 M35.3,29.9c0,3.5-2.2,5.8-5.3,5.8s-5.3-2.3-5.3-5.8
					s2.2-5.8,5.3-5.8S35.3,26.4,35.3,29.9 M27.5,29.9c0,2,1,3.3,2.5,3.3s2.5-1.3,2.5-3.3s-1.1-3.3-2.5-3.3
					C28.5,26.6,27.5,27.9,27.5,29.9 M46,24.3h-2.8L41,32.2l-2.1-7.9h-2.8l3.3,11.1h3.1l2.1-7.6l2.2,7.6h3.1l3.3-11.1h-2.8l-2.1,7.9
					L46,24.3z M57.5,20h-2.7v15.4h2.6L57.5,20z M69.6,29.9c0,0.3,0,0.7,0,0.9h-7c0.2,1.8,1.2,2.6,2.6,2.6c0.9,0,1.8-0.3,2.5-1l1.6,1.8
					c-1.1,1.1-2.6,1.5-4.2,1.5c-3.1,0-5.2-2.2-5.2-5.8s2-5.9,5-5.9C67.7,24,69.6,26.3,69.6,29.9 M62.6,28.8h4.3
					c-0.2-1.5-0.9-2.4-2.1-2.4C63.5,26.4,62.8,27.3,62.6,28.8 M78.8,25.3V20h2.7v15.4h-2.6v-1.1c-0.7,0.8-1.6,1.3-3,1.3
					c-2.7,0-4.7-2.4-4.7-5.8s2-5.8,4.7-5.8C77.2,24,78.1,24.5,78.8,25.3 M74,29.9c0,1.9,1,3.3,2.5,3.3s2.5-1.3,2.5-3.3s-1-3.3-2.5-3.3
					C74.9,26.6,74,27.9,74,29.9 M91.3,25.4v-1H94v10.4c0,3.3-2.1,5.3-5.3,5.3c-1.7,0-3.3-0.5-4.3-1.4l1.3-2c0.9,0.7,1.8,1,2.9,1
					c1.6,0,2.7-0.9,2.7-2.7v-1c-0.7,0.8-1.6,1.3-2.9,1.3c-2.7,0-4.7-2.2-4.7-5.6s1.9-5.6,4.7-5.6C89.7,24,90.7,24.6,91.3,25.4
					 M86.6,29.7c0,1.8,0.9,3.1,2.4,3.1s2.4-1.2,2.4-3.1s-1-3.1-2.4-3.1C87.5,26.6,86.6,27.9,86.6,29.7 M105.9,29.9c0,0.3,0,0.7,0,0.9h-7
					c0.2,1.8,1.2,2.6,2.6,2.6c0.9,0,1.8-0.3,2.5-1l1.6,1.8c-1.1,1.1-2.6,1.5-4.2,1.5c-3.1,0-5.2-2.2-5.2-5.8s2-5.9,5-5.9
					C103.9,24,105.9,26.3,105.9,29.9 M98.9,28.8h4.3c-0.2-1.5-0.9-2.4-2.1-2.4C99.8,26.4,99.1,27.3,98.9,28.8"/>
				</svg>
			</svg>
		</a>
		<i class="nav-mobile-hamburger sficon sficon-menu"></i>
	</div>
</nav>
<?php get_search_form(); ?>