<div class="thestacksgrid">
	<?php 
		$featured_query = new WP_Query(array(
			'posts_per_page' => -1,
			'post_type' => 'post',
			'cat' => 9,
		));

		$main_query = new WP_Query(array(
			'posts_per_page' => -1,
			'post_type' => 'post',
		));
	?>
	<div class="thestacksgrid-inpagenav">
		<div class="thestacksgrid-inpagenav-categories">
			<h2 class="thestacksgrid-inpagenav-categories-header">Categories</h2>
			<div class="thestacksgrid-inpagenav-categories-list">
				<?php 
					$categories = get_terms('category', array(
						'exclude' => 9,
					));
					if( !empty($categories) ):
						foreach( $categories as $category ):
				?>		
					<a href="<?php echo get_category_link($category->term_id); ?>" class="thestacksgrid-inpagenav-categories-list-item"><?php echo $category->name; ?></a>
				<?php
						endforeach;
				else: ?>
					There are no categories.
				<?php endif; ?>
			</div>
		</div>
		<div class="thestacksgrid-inpagenav-twitter">
			<h2 class="thestacksgrid-inpagenav-twitter-header">Twitter</h2>
			<div class="thestacksgrid-inpagenav-twitter-feed">
				<?php 
					echo do_shortcode( '[custom-twitter-feeds]' );
				?>
			</div>
		</div>
	</div>
	<div class="thestacksgrid-main">
		<?php if( $main_query->have_posts() ): ?>
		<div class="thestacksgrid-main-grid">
			<?php while( $main_query->have_posts() ): $main_query->the_post(); ?>
				<div class="thestacksgrid-main-grid-post">
					<a href="<?php the_permalink(); ?>" class="thestacksgrid-main-grid-post-imagecontainer">
						<img src="<?php echo ThemeTools::get_image_for_bloggrid('full') ?>" class="thestacksgrid-main-grid-post-imagecontainer-image">
					</a>
					<div class="thestacksgrid-main-grid-post-textcontainer">
						<?php 
							$categories = get_the_category( $post->ID );
							if( !empty($categories) ):
						?>
						<div class="thestacksgrid-main-grid-post-textcontainer-categories">
							<?php foreach( $categories as $category ): ?>
								<a href="<?php echo get_category_link( $category->term_id ); ?>" class="thestacksgrid-main-grid-post-textcontainer-categories-category"><?php echo $category->name; ?></a>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
						<a href="<?php the_permalink(); ?>" class="thestacksgrid-main-grid-post-textcontainer-title"><?php the_title(); ?></a>
						<div class="thestacksgrid-main-grid-post-textcontainer-author"><?php echo 'By ' . get_the_author(); ?></div>
						<?php 
							$tags = wp_get_post_tags( $post->ID );	
							if( !empty($tags) ):
						?>
						<div class="thestacksgrid-main-grid-post-textcontainer-tags">
							<?php foreach( $tags as $tag ): ?>
								<a href="<?php echo get_tag_link( $tag->term_id ); ?>" class="thestacksgrid-main-grid-post-textcontainer-tags-tag"><?php echo $tag->name; ?></a>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</div>