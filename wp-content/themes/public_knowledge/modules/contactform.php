<section class="contact section">
	<?php 
		if( empty(get_field('disable_inpagenav')) ){
			include(locate_template( 'modules/inpagenav-nav.php' )); 
		}
	?>
	<?php 
		echo apply_filters( 'the_content', $post->post_content );
	?>
	<div class="contact-wrapper">
		<div class="contact-wrapper-header">Join our mailing list:</div>
		<form method="post" class="contact-wrapper-form" action="<?php echo get_permalink(); ?>">
			<input class="contact-wrapper-form-email" type="email" name="email">
			<input class="contact-wrapper-form-submit" type="submit" name="submit">
		</form>
		<div class="contact-wrapper-messages">
			
		</div>
	</div>
</section>