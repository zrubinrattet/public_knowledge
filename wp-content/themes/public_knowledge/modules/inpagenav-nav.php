<?php 
$i = 0;
while( $i <= 1 ): ?>
<section class="inpagenav<?php echo $i == 1 ? ' dummy' : ''; ?><?php echo get_field('disable_hero') ? ' hasnohero' : ' hashero'; ?>">
	<h2 class="inpagenav-headercontainer">
		<div class="inpagenav-headercontainer-header">
			<?php 
			if( is_single() ){
				echo $post->post_title;
			}
			else{
				echo bloginfo('sitename'); 
			}

			?>
		</div>
	</h2>
	<div class="inpagenav-menu">
		<style type="text/css">
			div.inpagenav-headercontainer-header{
				color: <?php echo !empty(get_field('theme-color-tint', 'option')) ? ColorBeast::darken(get_field('theme-color-tint', 'option'), 0.15) : '#333333'; ?>;
			}
			a.inpagenav-menu-item{
				color: <?php echo !empty(get_field('theme-color-tint', 'option')) ? get_field('theme-color-tint', 'option') : '#777777'; ?>;
			}
			a.inpagenav-menu-item:hover{
				color: <?php echo !empty(get_field('theme-color-tint', 'option')) ? ColorBeast::darken(get_field('theme-color-tint', 'option'), 0.4) : '#000000'; ?>;
			}
		</style>
		<?php 
			if( is_single() ){
				$items = get_terms( 'category' );
			}
			else{
				$items = wp_get_nav_menu_items( 'in-page-nav' ); 
				if( $items === false || empty($items) ){
					$items = wp_get_nav_menu_items( 'inpage-nav' );
					if( empty($items) ){
						$items = wp_get_nav_menu_items( 'sidebar-nav' );
					}
				}
			}
			foreach( $items as $item ):
				if( is_single() ){
					if( strtolower($item->name) !== 'featured' ){
						?>
							<a href="<?php echo get_term_link( $item, 'category' ); ?>" class="inpagenav-menu-item"><?php echo $item->name ?></a>
						<?php
					}
				}
				else{	
				?>
					<a href="<?php echo $item->url ?>" class="inpagenav-menu-item"><?php echo $item->title ?></a>
				<?php 
				}
			endforeach; 
		?>
			<a href="#" class="inpagenav-menu-item backtotop">Back to Top</a>
	</div>
</section>
<?php $i++;

endwhile; ?>