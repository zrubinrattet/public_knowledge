<?php
if( empty(get_field('disable_inpagenav')) ){
	include(locate_template( 'modules/inpagenav-nav.php' )); 
}
?>
<section class="section general">
	<?php 
		if( !isset($post) ){
			global $post;
		}
		echo apply_filters( 'the_content', $post->post_content ); 
	?>
</section>