<div class="bloggrid">
<?php 

	require_once(get_template_directory() . '/classes/ThemeTools.php');


	while($the_query->have_posts()): $the_query->the_post();

		ThemeTools::the_bloggrid_item($post);
	
	endwhile; ?>
</div>
<?php
if( get_current_blog_id() !== 1 && $the_query->found_posts > 10 ){
	?>	
		<div class="seemore">
			<img src="<?php echo admin_url( 'images/spinner-2x.gif' ); ?>" class="seemore-loader">
			<div class="seemore-button">See more</div>
		</div>
	<?php
}
?>