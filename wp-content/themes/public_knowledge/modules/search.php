<?php 

$search_proj = isset($_GET['s_proj']) ? true : false;

if(!$search_proj){
	$the_query = new WP_Query_Multisite(array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'sites' => array(
			'public' => 1,
		),
		's' => get_search_query(),
	));
}
else{
	$the_query = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		's' => get_search_query(),
	));
}

?>
<section class="search section">
<?php

if($the_query->have_posts()):
?>
	
	<?php include(locate_template('modules/bloggrid.php')); ?>

<?php 
else:
?>
	<div class="search-noresults">
		Sorry.<br/>No results match your query.
	</div>
<?php
endif;
?>
</section>