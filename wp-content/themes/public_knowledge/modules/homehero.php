<section class="homehero">
	<?php if(!is_main_site( get_current_blog_id() )): ?>
		<div class="homehero-title">
			<div class="homehero-title-top" style="background-color: <?php echo get_field('theme-color-tint-toggle', 'option') ? get_field('theme-color-tint', 'option') : '#ffffff'; ?>"></div>
			<div class="homehero-title-middle" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/img/project-<?php echo get_current_blog_id() ?>-<?php echo get_blog_option(get_current_blog_id(), 'current_project_texthero_hash'); ?>-text.png');"></div>
			<div class="homehero-title-bottom" style="background-color: <?php echo get_field('theme-color-tint-toggle', 'option') ? get_field('theme-color-tint', 'option') : '#ffffff'; ?>"></div>
		</div>
	<?php endif; ?>
	<div class="homehero-bgslider">
		<?php 
		if(is_main_site( get_current_blog_id() )):
			$bg_urls = ThemeTools::get_site_bg_images();
			foreach($bg_urls as $bg_url):
				if( !empty($bg_url['image_url']) && $bg_url['blog_id'] != 1 ):
			?>
				<div class="homehero-bgslider-item" style="background-image: url('<?php echo $bg_url['image_url']; ?>');">
					<?php switch_to_blog($bg_url['blog_id']); ?>
						<h2 class="homehero-bgslider-item-header"<?php echo get_field('theme-color-tint-toggle', 'option') ? ' style="color: ' . get_field('theme-color-tint', 'option') . ';"' : '' ?>><?php echo get_blog_details($bg_url['blog_id'])->blogname; ?></h2>
						<div class="homehero-bgslider-item-tint"></div>
					<?php restore_current_blog(); ?>
				</div>
			<?php 
				endif;
			endforeach; 
		else:
			$bg_url = get_field('hero_bg_image', 'option');
			if( !empty($bg_url) ):	
		?>
			<div class="homehero-bgslider-item" style="background-image: url('<?php echo get_field('hero_bg_image', 'option'); ?>');"></div>
		<?php 
			endif;
		endif; ?>
	</div>
	<?php if(is_main_site( get_current_blog_id() )): ?>
		<i class="homehero-arrow fa fa-angle-down"></i>
	<?php endif; ?>
</section>
<?php if(!is_main_site( get_current_blog_id() )): ?>
	<?php if( !get_field('disable_descriptionspacer', $post->ID) ): ?>
		<section class="homespacer">
			<div class="homespacer-spacer"></div>
			<div class="homespacer-descriptioncontainer" style="background-color: <?php echo get_field('theme-color-tint-toggle', 'option') ? get_field('theme-color-tint', 'option') : '#ffffff'; ?>">
				<div class="homespacer-descriptioncontainer-description"><?php echo get_bloginfo('description'); ?></div>
			</div>
		</section>
	<?php endif; ?>
<?php endif; ?>