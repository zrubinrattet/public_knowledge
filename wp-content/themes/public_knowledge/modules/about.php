<?php 
global $post;

if( empty(get_field('disable_inpagenav') ) ){
	include(locate_template( 'modules/inpagenav-nav.php' )); 
}
?>

<section class="about section wysiwyg">
	<div class="about-content wysiwyg-content">
		<?php echo apply_filters('the_content', $post->post_content); ?>
	</div>
</section>