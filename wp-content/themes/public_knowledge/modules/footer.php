<footer class="footer section">	
	<?php
		switch_to_blog(1);
		if( !empty(get_field('sponsor-logo', 'option')['url']) ):
	?>
		<img src="<?php echo get_field('sponsor-logo', 'option')['url']; ?>" class="footer-sponsor">
	<?php endif; ?>
	<div class="footer-creditjoin">
		<div class="footer-creditjoin-credit"><?php the_field('footer-credit', 'option'); ?></div>
		<div class="footer-creditjoin-join">
			<div class="footer-creditjoin-join-button">Join the Newsletter</div>
			<div class="footer-creditjoin-join-signup">
				<div class="footer-creditjoin-join-signup-wrapper">
					<i class="footer-creditjoin-join-signup-wrapper-close sficon sficon-close"></i>
					<?php sfmoma_ns_form('footer-creditjoin-join-signup-wrapper-form', 'footer-creditjoin-join-signup-wrapper-form-input'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-sitemap">
		<a target="_blank" href="<?php echo get_field('terms-of-use', 'option')['url']; ?>" class="footer-sitemap-link">Terms of Use</a>
		<span class="footer-sitemap-pipe n1">|</span>
		<a target="_blank" href="<?php echo get_field('privacy-policy', 'option')['url']; ?>" class="footer-sitemap-link">Privacy Policy</a>
		<span class="footer-sitemap-pipe n2">|</span>
		<span class="footer-sitemap-copyright">Copyright &copy; <?php echo Date('Y') . ' SFMOMA. All rights reserved.'?></span>
		<a target="_blank" href="<?php echo bloginfo('rss_url'); ?>" class="footer-sitemap-link rss">RSS</a>
	</div>
</footer>
<?php restore_current_blog(); ?>