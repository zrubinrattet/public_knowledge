<?php global $post; ?>
<section class="singletitle section">
	<div class="bloggrid">
		<div class="bloggrid-item">
			<div class="bloggrid-item-postcontainer">
				<div class="bloggrid-item-postcontainer-post">
					<?php if( !in_array( $post->post_type, array('event', 'location') ) ): ?>
						<h5 class="bloggrid-item-postcontainer-post-date"><?php echo date('F j, Y', strtotime($post->post_date)); ?></h5>
					<?php endif; ?>
					<a href="<?php the_permalink(); ?>" class="bloggrid-item-postcontainer-post-header"><?php echo $post->post_title; ?></a>
					<?php if( !in_array( $post->post_type, array('event', 'location') ) ): 
							$user = get_user_by( 'ID', $post->post_author );
							if( $user->display_name != 'Public Knowledge' || !get_field('post_has_no_author', $post->ID) ):
							?>
								<h5 class="bloggrid-item-postcontainer-post-author">By <?php echo get_the_author_meta('display_name', $post->post_author); ?></h5>
						<?php 
							endif;
						endif; ?>
					<div class="bloggrid-item-postcontainer-post-share">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink($post->ID) ); ?>" target="_blank" class="bloggrid-item-postcontainer-post-share-link">
							<i class="bloggrid-item-postcontainer-post-share-link-icon sficon sficon-facebook"></i>
						</a>
						<a href="https://twitter.com/home?status=<?php echo urlencode( get_permalink($post->ID) ); ?>" target="_blank" class="bloggrid-item-postcontainer-post-share-link">
							<i class="bloggrid-item-postcontainer-post-share-link-icon sficon sficon-twitter"></i>
						</a>
						<a href="<?php echo 'mailto:?body=' . get_permalink($post->ID); ?>" class="bloggrid-item-postcontainer-post-share-link share">
							<i class="bloggrid-item-postcontainer-post-share-link-icon sficon sficon-share"></i>
						</a>
					</div>
					<div class="bloggrid-item-postcontainer-post-content">
						<?php 
							if( $post->post_type === 'event' ){
								$emutil = new EMUtil();
								// category stuff
								$emutil->render_event_category_list_as_links($emutil->get_event_category_list($post->ID));

								echo '<br/><br/>';
								// datetime stuff
								$event = $emutil->get_event($post->ID)[0];

								// location stuff
								$location = $emutil->get_event_location($post->ID);

								?>
									<a target="_blank" href="<?php echo 'https://www.google.com/maps/search/?api=1&query=' . urlencode($location['location_address'] . ' ' . $location['location_postcode']); ?>"><?php echo $location['location_name']; ?><br/><?php echo $location['location_address'] . ', ' . $location['location_town'] . ' ' . $location['location_state'] . ', ' . $location['location_postcode'] ; ?></a>
									<br/>
									<br/>
								<?php
								echo wpautop( $event['post_content'] );
							}
							else{
								echo do_shortcode( apply_filters('the_content', $post->post_content) ); 
							}
						?>
					</div>
					<?php 
						$tags = wp_get_post_tags( $post->ID );
						if( !empty($tags) ):
					?>
						<div class="bloggrid-item-postcontainer-post-tags">
							<?php foreach( $tags as $tag ): ?>
								<a href="<?php echo get_term_link( $tag ); ?>" class="bloggrid-item-postcontainer-post-tags-tag"><?php echo $tag->name; ?></a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<div class="bloggrid-item-postcontainer-post-share">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink($post->ID) ); ?>" target="_blank" class="bloggrid-item-postcontainer-post-share-link">
							<i class="bloggrid-item-postcontainer-post-share-link-icon sficon sficon-facebook"></i>
						</a>
						<a href="https://twitter.com/home?status=<?php echo urlencode( get_permalink($post->ID) ); ?>" target="_blank" class="bloggrid-item-postcontainer-post-share-link">
							<i class="bloggrid-item-postcontainer-post-share-link-icon sficon sficon-twitter"></i>
						</a>
						<a href="<?php echo 'mailto:?body=' . get_permalink($post->ID); ?>" class="bloggrid-item-postcontainer-post-share-link share">
							<i class="bloggrid-item-postcontainer-post-share-link-icon sficon sficon-share"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<?php if( get_field('show_artist_blurb', $post->ID) ): ?>
			<div class="bloggrid-artistblurb">
				<div class="bloggrid-artistblurb-imagecontainer">

					<?php if(is_super_admin( $post->post_author )): 
						switch_to_blog(1);
					?>
						<img src="<?php echo wp_get_attachment_url( get_user_meta( $post->post_author, 'user_profile_image', true ) ); ?>" class="bloggrid-artistblurb-imagecontainer-image">
					<?php 
						restore_current_blog();
						else: ?>
						<img src="<?php echo wp_get_attachment_url( get_user_meta( $post->post_author, 'user_profile_image', true ) ); ?>" class="bloggrid-artistblurb-imagecontainer-image">
					<?php endif; ?>
				</div>
				<div class="bloggrid-artistblurb-blurbcontainer">
					<div class="bloggrid-artistblurb-blurbcontainer-blurb">
						<?php the_author_meta( 'description', $post->post_author ); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>