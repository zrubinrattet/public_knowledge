<?php 
global $post;
?>

<section class="raw section wysiwyg">
	<div class="raw-content wysiwyg-content">
		<?php echo do_shortcode(apply_filters('the_content', $post->post_content)); ?>
	</div>
</section>