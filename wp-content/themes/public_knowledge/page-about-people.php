<?php

/**
 * Template Name: People Page
 *
 */

get_header();

include(locate_template( 'modules/nav.php' ));

if( empty(get_field('disable_hero')) ){
	include(locate_template( 'modules/homehero.php' ));
}

?>
<div class="mainBackgroundColor">
	<?php
		if( empty(get_field('disable_inpagenav') ) ){
			include(locate_template( 'modules/inpagenav-nav.php' )); 
		}
		if( have_rows('people_repeater_group', $post->ID) ): ?>
			<div class="people">
				<?php
				$hero_image = get_field('people_hero_image', $post->ID);
				if( !empty($hero_image) ):?>
					<img class="people-hero" src="<?php echo $hero_image['sizes']['large']; ?>">
				<?php
				endif;?>
				<?php
					while( have_rows('people_repeater_group', $post->ID) ): the_row();?>
						<div class="people-group">
							<h2 class="people-group-header"><?php the_sub_field('people_header'); ?></h2>
							<?php if( have_rows('people_repeater', $post->ID) ): ?>
								<div class="people-group-list">
									<?php while( have_rows('people_repeater', $post->ID) ): the_row(); ?>
										<div class="people-group-list-item">
											<div class="people-group-list-item-imagecontainer">
												<img src="<?php echo get_sub_field('person_image')['sizes']['medium']; ?>" class="people-group-list-item-imagecontainer-image">
											</div>
											<div class="people-group-list-item-textcontainer">
												<div class="people-group-list-item-textcontainer-text">
													<?php
														$link = get_sub_field('person_link');
														if( !empty($link) ): ?>
															<a href="<?php echo $link['url']; ?>" class="people-group-list-item-textcontainer-text-name"><?php the_sub_field('person_name') ?></a>
													<?php
														else:
													?>
														<span class="people-group-list-item-textcontainer-text-name"><?php the_sub_field('person_name'); ?></span>
													<?php endif;
													the_sub_field('person_bio');
													?>
												</div>
											</div>
										</div>
									<?php endwhile; ?>
								</div>
							<?php endif; ?>
						</div>
				<?php endwhile; ?>
			</div>
				<?php
		endif;

		include(locate_template( 'modules/footer.php' ));
	?>
</div>	
<?php get_footer(); ?>