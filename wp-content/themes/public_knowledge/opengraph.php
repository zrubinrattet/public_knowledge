<!-- Place this data between the <head> tags of your website -->
<meta name="description" content="<?php echo get_bloginfo('description'); ?>" />

<?php 
	if ( is_single() ){
		$title = $post->post_title;
		$description = wp_trim_words( strip_tags( $post->post_excerpt ), 20, '...' );
		$image = ThemeTools::get_image_for_opengraph();
	}
	else{
		$title = get_bloginfo();
		$description = get_bloginfo( 'description' );
		$image = get_template_directory_uri() . '/library/img/logo-black.png';	
	}
?>

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $title; ?>">
<meta itemprop="description" content="<?php echo $description; ?>">
<meta itemprop="image" content="<?php echo $image; ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sfmoma">
<meta name="twitter:title" content="<?php echo $title ?>">
<meta name="twitter:description" content="<?php echo $description; ?>">
<meta name="twitter:creator" content="@sfmoma">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="<?php echo $image; ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $title ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php 
	global $wp;
	echo home_url(add_query_arg(array(),$wp->request)); 
?>" />
<meta property="og:image" content="<?php echo $image; ?>" />
<meta property="og:description" content="<?php echo $description ?>" />
<meta property="og:site_name" content="<?php echo get_bloginfo(); ?>" />