<?php 
/**
 * Template Name: Blog Page
 *
 */ 

get_header();

include(locate_template( 'modules/nav.php' ));

if( empty(get_field('disable_hero') )){
	include(locate_template( 'modules/homehero.php' ));
}
?>
<div class="mainBackgroundColor">
	<?php
		if( empty(get_field('disable_inpagenav')) ){
			include(locate_template( 'modules/inpagenav-nav.php' )); 
		}
		include(locate_template( 'modules/blog.php' ));
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>