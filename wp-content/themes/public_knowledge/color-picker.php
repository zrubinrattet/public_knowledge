<?php 

$tint_color = get_field('theme-color-tint', 'option');


if(get_field('theme-color-tint-toggle', 'option')):
?>

<style type="text/css">
	@media only screen {
		body,
		.mainBackgroundColor{
			background-color: <?php echo $tint_color; ?>;
		}	
		.projectsite .mainBackgroundColor,
		.singlepost .mainBackgroundColor{
			background-color: #f8f8f8;
		}
		.hastint .nav--belowhero{
			background-color: <?php echo $tint_color; ?>;
		}
		.hastint .nav-wrapper-tint{
			background-color: <?php echo ColorBeast::get_rgba($tint_color, 0.9); ?>;
		}
		.nav-mobile--sticky{
			background-color: <?php echo ColorBeast::get_rgba($tint_color, 0.9); ?>;
			border-bottom: 1px solid <?php echo ColorBeast::darken($tint_color, 0.1); ?>;
		}
		.nav-wrapper-items-right-search-dropdown{
			background-color: <?php echo $tint_color; ?>;
		}
		.homehero-titlecontainer{
			background-color: <?php echo $tint_color; ?>;
		}
		.bloggrid-item-authorcontainer-author-description{
			border-color: <?php echo $tint_color; ?>;	
		}
		.bloggrid-item-postcontainer-post-elipsis,
		.bloggrid-item-postcontainer-post-header{
			color: <?php echo $tint_color; ?>;
		}
		.searchform-tint{
			background-color: <?php echo $tint_color; ?>;
		}
		.homehero-descriptioncontainer-description{
			color: <?php echo $tint_color ?>;
		}
	}
	@media only screen and (min-width: 1025px){
		.nav-wrapper--sticky .nav-wrapper-tint{
			background-color: <?php echo ColorBeast::get_rgba($tint_color, 0.95); ?>;
			border-bottom: 1px solid <?php echo ColorBeast::darken($tint_color, 0.1); ?>;
		}
	}
</style>

<?php endif; ?>