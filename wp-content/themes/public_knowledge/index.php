<?php get_header();
$has_hero = true;
include(locate_template( 'modules/nav.php' ));
include(locate_template( 'modules/homehero.php' ));
?>
<div class="mainBackgroundColor">
	<?php
		if(is_main_site(get_current_blog_id())):
			include(locate_template( 'modules/projects.php' ));
		else:
			if( empty(get_field('disable_inpagenav') ) ){
				include(locate_template( 'modules/inpagenav-nav.php' )); 
			}
			if( get_blog_details( get_current_blog_id() )->path == '/does-art-have-users/' ){
				$post = get_page_by_title('info');
				include(locate_template( 'modules/general.php' ));
			}
			else{
				include(locate_template( 'modules/blog.php' ));
			}
		endif;
		include(locate_template( 'modules/footer.php' ));
	?>
</div>

<?php
get_footer(); ?>