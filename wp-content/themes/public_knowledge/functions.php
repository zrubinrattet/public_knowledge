<?php 

// add wp_query_multisite
require('classes/WP_Query_Multisite.php');
// add class that has all the fancy color translation functions
require('classes/ColorBeast.php');
// Events Manager Plugin Utility
require('classes/EMUtil.php');
// Events Manager Plugin Seemore
require('classes/EMSeemore.php');

// Basic Theme Setup
require('classes/SetupTheme.php');
// Tools Used in Theme
require('classes/ThemeTools.php');
// Customize Front End of Theme
require('classes/CustomizeFrontend.php');
// Customize Back End of Theme
require('classes/CustomizeBackend.php');
// Customize Editor
require('classes/CustomizeEditor.php');
// Handle Email Submissions
require('classes/EmailHandler.php');

// Custom Fields
require('acf.php');

?>