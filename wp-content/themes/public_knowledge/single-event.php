<?php
	get_header();
?>

<div class="mainBackgroundColor">
	<?php include(locate_template( 'modules/nav.php' )); ?>
	<section class="eventsingle">
		<h1 class="eventsingle-title"><?php echo $post->post_title; ?></h1>
		<?php
			$cats = wp_get_object_terms( $post->ID, 'event-categories' );
			if( !empty($cats) ):
		?>
			<div class="eventsingle-cats">
				<?php foreach( $cats as $cat ): ?>
					<a href="<?php echo get_term_link( $cat->term_id, 'event-categories' ); ?>" class="eventsingle-cats-cat"><?php echo $cat->name; ?></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<div class="eventsingle-date">
			<?php
				$emutil = new EMUtil();
				echo $emutil->get_datetimerange($post->ID);
			?>
		</div>
		<?php
			global $wpdb;
			
			$location_id = get_post_meta($post->ID, '_location_id', true);
			
			$location = $wpdb->get_results("select * from " . $wpdb->prefix . "em_locations where location_id = " . strval($location_id), ARRAY_A)[0];

			$location_text = $location['location_name'] . '<br/>' . $location['location_address'] . ' ' . $location['location_town'] . ', ' . $location['location_state'] . ' ' . $location['location_postcode'];

			$location_href = '//google.com/maps/search/' . urlencode($location['location_address'] . ' ' . $location['location_town'] . ' ' . $location['location_state']);
		?>
		<a target="_blank" href="<?php echo $location_href; ?>" class="eventsingle-location"><?php echo $location_text; ?></a>
		<?php 
			$event = $wpdb->get_results('select * from ' . $wpdb->prefix . 'em_events where post_id = ' . $post->ID, ARRAY_A)[0];
		?>
		<div class="eventsingle-notes"><?php echo get_post_meta( $post->ID, 'Notes', true ); ?></div>
		<?php
			$facebook_event_url = get_post_meta($event['post_id'], 'Facebook Event URL', true);
			if( !empty($facebook_event_url) ):
		?>
			<a href="<?php echo $facebook_event_url ?>" class="eventsingle-facebookurl">RSVP on Facebook</a>
		<?php endif; ?>
		<div class="eventsingle-content"><?php echo wpautop( $post->post_content ); ?></div>
	</section>
</div>

<?php
	include(locate_template( 'modules/footer.php' ));
	get_footer();
?>