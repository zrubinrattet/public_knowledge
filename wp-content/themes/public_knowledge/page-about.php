<?php 
/**
 * Template Name: About Page
 *
 */ 

get_header();

include(locate_template( 'modules/nav.php' ));

if( empty(get_field('disable_hero')) ){
	include(locate_template( 'modules/homehero.php' ));
}
?>
<div class="mainBackgroundColor">
	<?php
		include(locate_template( 'modules/about.php' ));
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>