<?php 
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'general-settings',
		'icon_url'      => 'dashicons-admin-settings',
		'redirect'		=> false,
	));
}


function add_acf_fields() {

	$the_stacks_page = get_page_by_path( 'the-stacks' );

	acf_add_local_field_group(array(
		'key' => 'group_120387d8',
		'title' => 'Page Options',
		'fields' => array(
			array(
				'key' => 'field_2813yhuaae',
				'label' => 'Disable In-Page Nav?',
				'type' => 'true_false',
				'name' => 'disable_inpagenav',
				'ui' => true,
			),
			array(
				'key' => 'field_28uaae',
				'label' => 'Disable Hero?',
				'type' => 'true_false',
				'name' => 'disable_hero',
				'ui' => true,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
				array(
					'param' => 'page',
					'operator' => '!=',
					'value' => $the_stacks_page->ID,
				),
			),
		),
	));

	acf_add_local_field_group(array(
		'key' => 'group_1',
		'title' => 'Misc.',
		'fields' => array (
			array(
				'key' => 'field_81621321',
				'label' => 'Hero Background Image',
				'name' => 'hero_bg_image',
				'type' => 'image',
				'return_format' => 'url',
			),
			array(
				'key' => 'field_hero_slider_order',
				'label' => 'Hero Slider Order',
				'instructions' => 'Higher number means sooner',
				'name' => 'hero_slider_order',
				'type' => 'number',
			),
			array(
				'key' => 'field_nnadhaef',
				'label' => 'Use Theme Color Tint?',
				'name' => 'theme-color-tint-toggle',
				'type' => 'true_false',
				'wrapper' => array(
					'width' => '15',
				),
			),
			array(
				'key' => 'field_77zchasdfa',
				'label' => 'Theme Color Tint',
				'name' => 'theme-color-tint',
				'type' => 'color_picker',
				'wrapper' => array(
					'width' => '25',
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'general-settings',
				),
			),
		),
	));

	acf_add_local_field_group(array(
		'key' => 'group_2',
		'title' => 'Misc. Post Settings',
		'fields' => array(
			array(
				'key' => 'field_hheahf',
				'label' => 'Archive Post?',
				'name' => 'is_post_archived',
				'type' => 'true_false',
			),
			array(
				'key' => 'field_hn83h121',
				'label' => 'Post has no author?',
				'name' => 'post_has_no_author',
				'type' => 'true_false',
				'instructions' => 'Tick this box if you want no profile image/description to show up on the post when it\'s returned anywhere on the site.', 
			),
			array(
				'key' => 'field_)0v8zyohids',
				'label' => 'Show Artist Blurb?',
				'name' => 'show_artist_blurb',
				'type' => 'true_false',
				'instructions' => 'Tick this box if you want the artist blurb to show up at the bottom.',
			),
		), 
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post'
				),
			),
		),
	));
	
	$id;

	if(isset($_GET['post'])) $id = $_GET['post'];

	acf_add_local_field_group(array(
		'key' => 'group_4',
		'title' => ' ',
		'fields' => array(
			array(
				'key' => 'field_y21128ha',
				'label' => 'Slider Header',
				'type' => 'text',
				'name' => 'slider_header',
			),
			array(
				'key' => 'field_nn82y123',
				'label' => 'Slides',
				'name' => 'slider_gallery',
				'type' => 'gallery',
				'button_label' => 'Add To Slider',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfmomapk_slider',
				),
			),
		),
	));

	if( isset($id) ):
		acf_add_local_field(array(
			'key' => 'field_y12y123',
			'label' => '<div style="cursor:text;">
						This is your slider shortcode. Copy paste this into your post.
						<br/><br/>
						<div style="font-size: 18px; font-weight: 300; background-color: #eeeeee; padding: 20px;">
							[sfmomapkslider id="' . $id . '"]
						</div>
						</div>',
			'type' => 'message',
			'name' => 'slider_shortcode',
			'parent' => 'group_4',
		));
	endif;

	if(get_current_blog_id() == 1){
		acf_add_local_field_group(array(
			'key' => 'group_5',
			'title' => 'Calendar Embed',
			'fields' => array(
				array(
					'key' => 'field_n712haafa',
					'label' => 'Google API Key',
					'instructions' => 'To get this credential you\'ll need to log into console.developers.google.com with the public knowledge google account.',
					'type' => 'text',
					'name' => 'google_api_key',
				),
				array(
					'key' => 'field_n1721gefaf',
					'label' => 'Google Calendar ID',
					'instructions' => 'To get this you\'ll need to log into calendar.google.com with the public knowledge google account. Then click on the arrow next to the calendar you want the id from and select "Calendar Settings". You\'ll find the calendar ID in the "Calendar Address" section.',
					'type' => 'text',
					'name' => 'google_calendar_id',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'page-calendar.php',
					),
				),
			),
		));

		acf_add_local_field_group(array(
			'key' => 'group_289137ds',
			'title' => 'The Stacks Options',
			'fields' => array(
				array(
					'key' => 'field_8aifdhia',
					'name' => 'stacks_bg_image',
					'label' => 'Tile BG Image',
					'type' => 'image',
					'return_format' => 'url',
					'instructions' => 'This is the image that appears on the tile in the projects page.',
				),
				array(
					'key' => 'field_zhv87h123',
					'name' => 'stacks_tile_color',
					'label' => 'Tile BG Color',
					'type' => 'color_picker',
					'instructions' => 'This is the color that appears on the projects page when you hover over a tile and see the description (but only for The Stacks tile!).',
				),
				array(
					'key' => 'field_8zcvhuew32',
					'name' => 'stacks_tile_description',
					'label' => 'Tile Description',
					'type' => 'textarea',
					'instructions' => 'This is the description that appears on the tile in the projects page. Keep it short & sweet!',
				),
			),
			'location' => array(
				array(
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'general-settings',
					),
				),
			),
		));

		acf_add_local_field_group(array(
			'key' => 'group_09zvcjbewdasf',
			'title' => 'Social Links',
			'fields' => array(
				array(
					'key' => 'field_0zoiv89y12',
					'label' => 'Facebook',
					'type' => 'url',
					'name' => 'facebook_url',
				),
				array(
					'key' => 'field_0zooiawfe12',
					'label' => 'Twitter',
					'type' => 'url',
					'name' => 'twitter_url',
				),
				array(
					'key' => 'field_afdaefasd123',
					'label' => 'Instagram',
					'type' => 'url',
					'name' => 'instagram_url',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'general-settings',
					),
				),
			),
		));

		acf_add_local_field_group(array(
			'key' => 'group_vhzouikjdfspa8hf',
			'title' => 'Footer Settings',
			'fields' => array(
				array(
					'key' => 'field_0uvzia8fhiuad',
					'label' => 'Sponsor Logo',
					'type' => 'file',
					'name' => 'sponsor-logo',
				),
				array(
					'key' => 'field_0oizhfhadsfa',
					'label' => 'Credit',
					'type' => 'textarea',
					'name' => 'footer-credit',
					'new_lines' => 'br',
				),
				array(
					'key' => 'field_92iuhfadhuidha',
					'label' => 'Terms of Use Link',
					'type' => 'link',
					'name' => 'terms-of-use',
				),
				array(
					'key' => 'field_123oidafih',
					'label' => 'Privacy Policy',
					'type' => 'link',
					'name' => 'privacy-policy',
					'return_value' => 'url',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'general-settings',
					),
				),
			),
		));

		acf_add_local_field_group(array(
			'key' => 'group_people_repeater',
			'title' => 'Manage people page here',
			'fields' => array(
				array(
					'key' => 'field_people_hero_image',
					'type' => 'image',
					'name' => 'people_hero_image',
					'label' => "Hero Image",
				),
				array(
					'key' => 'field_people_repeater_group',
					'name' => 'people_repeater_group',
					'label' => ' ',
					'type' => 'repeater',
					'button_label' => 'Add New Group of People',
					'layout' => 'row',
					'sub_fields' => array(
						array(
							'key' => 'field_people_header',
							'name' => 'people_header',
							'label' => 'Title of People Group',
							'type' => 'text',
						),
						array(
							'key' => 'field_people_repeater',
							'name' => 'people_repeater',
							'label' => 'List of People',
							'type' => 'repeater',
							'button_label' => 'Add New Person',
							'layout' => 'row',
							'sub_fields' => array(
								array(
									'key' => 'field_person_name',
									'label' => 'Name',
									'name' => 'person_name',
									'type' => 'text',
								),
								array(
									'key' => 'field_person_link',
									'label' => 'Link',
									'name' => 'person_link',
									'type' => 'link',	
								),
								array(
									'key' => 'field_person_image',
									'label' => 'Image',
									'name' => 'person_image',
									'type' => 'image',
								),
								array(
									'key' => 'field_person_bio',
									'type' => 'textarea',
									'label' => 'Bio',
									'name' => 'person_bio'
								),
							),
						),
					),
				),
				
			),
			'location' => array(
				array(
					array(
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'page-about-people.php',
					),
				),
			),
		));

		acf_add_local_field_group(array(
			'key' => 'group_098yohivzcx',
			'title' => 'Site Sort',
			'fields' => array(
				array(
					'key' => 'field_09218yiu2hewfa',
					'label' => ' ',
					'type' => 'repeater',
					'name' => 'site_sort',
					'sub_fields' => array(
						array(
							'key' => 'field_8ovxizcvkufds',
							'label' => 'Site Name',
							'type' => 'text',
							'readonly' => true,
							'name' => 'site_name'
						),
						array(
							'key' => 'field_o8uczvvda',
							'label' => 'Alternate Name',
							'type' => 'text',
							'name' => 'site_name_alt',
							'instructions' => 'If left blank Site Name will be used as the fallback',
						),
						array(
							'key' => 'field_u90ciovzhz',
							'label' => 'In Project Grid?',
							'type' => 'true_false',
							'name' => 'in_project_grid',
							'ui' => true,
							'default' => true,
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'general-settings',
					),
				),
			),
		));
	}
	else{
		acf_add_local_field(array(
			'key' => 'field_o8ddfha',
			'label' => 'Disable Description/Spacer?',
			'type' => 'true_false',
			'name' => 'disable_descriptionspacer',
			'ui' => true,
			'parent' => 'group_120387d8',
		));

		acf_add_local_field_group(array(
			'key' => 'group_o8dah',
			'title' => 'Post Options',
			'fields' => array(
				array(
					'key' => 'field_c8ovzyhiv',
					'label' => 'Disable In-Page Nav?',
					'type' => 'true_false',
					'name' => 'disable_inpagenav',
					'ui' => true,
					'default_value' => true,
				)
			),
			'location' => array(
				array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
					)
				)
			),
		));
	}
}

add_action('acf/init', 'add_acf_fields');

add_action('acf/init', 'handle_site_sort');

function handle_site_sort(){
	if( get_current_blog_id() == 1 ){
		$rows = get_field('site_sort', 'option');
		$all_sites = get_sites();
		$the_stacks = get_page_by_path('the-stacks');
		array_push($all_sites, $the_stacks);
		// if rows have been built
		if( !empty($rows) ){	
			foreach( $all_sites as $site ){
				$name;
				if( get_class($site) == 'WP_Site' ){
					$name = ucwords(str_replace(array('/', '-'), array('', ' '), $site->path));
				}
				else{
					$name = ucwords(str_replace(array('/', '-'), array('', ' '), $site->post_name));
				}

				$in_a_row = false;
				foreach( $rows as $row ){
					if( $row['site_name'] == $name ){
						$in_a_row = true;
						break;
					}
				}
				if( $in_a_row == false ){
					if( get_class($site) == 'WP_Site' ){
						if( !empty( str_replace(' ', '', $name) ) && $site->blog_id !== 1 ){
							add_row('site_sort', array(
								'site_name' => $name,
								'in_project_grid' => true,
							), 'option');
						}
					}
					else{
						if( !empty( str_replace(' ', '', $name) ) ){
							add_row('site_sort', array(
								'site_name' => $name,
								'in_project_grid' => true,
							), 'option');
						}
					}
				}
			}
		}
		// otherwise build the rows
		else{
			foreach( $all_sites as $site ){
				$name = ucwords(str_replace(array('/', '-'), array('', ' '), $site->path));
				if( !empty( str_replace(' ', '', $name) ) ){
					add_row('site_sort', array(
						'site_name' => $name,
						'in_project_grid' => true,
					), 'option');
				}
			}
		}
	}
}

add_action('acf/input/admin_footer', 'remove_acf_buttons');

function remove_acf_buttons() {
	
	?>
	<script type="text/javascript">
	(function($) {
		
		$('.acf-field-nn82y123 a.acf-button').text('Add To Slider');
		$('.acf-field-09218yiu2hewfa .acf-actions.acf-hl').hide();
		
	})(jQuery);	
	</script>
	<?php
	
}
?>