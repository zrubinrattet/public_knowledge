<?php 
/**
 * Template Name: Projects Page
 *
 */ 

get_header();
?>

<div class="mainBackgroundColor">
	<?php
		include(locate_template( 'modules/nav.php' ));
		include(locate_template( 'modules/projects.php' ));
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>