<?php get_header(); ?>
<?php 
	global $post;
	if( empty( get_field('disable_inpagenav') ) && is_main_site( get_current_blog_id() ) && $post->post_type !== 'event' ){
		include(locate_template( 'modules/inpagenav-nav.php' )); 
	}
?>
<div class="mainBackgroundColor">
	<?php
		include(locate_template( 'modules/nav.php' ));
		include(locate_template( 'modules/single.php' ));
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>