module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        files: {
          'build/css/build.css' : 'sass/main.scss',
        },
      },
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['js_vendor/jquery-3.3.1.min.js', 'js_vendor/baguettebox.min.js', 'js_vendor/isotope.pkgd.min.js', 'js_vendor/packery-mode.pkgd.min.js', 'js_vendor/jquery-ui/jquery-ui.min.js', 'js/**/*.js'],
        dest: 'build/js/build.js',
      },
    },
    watch: {
      sass: {
        files: ['sass/**/*.scss'],
        tasks: ['sass']
      },
      js: {
        files: ['js/**/*.js'],
        tasks: ['concat']
      },
      php: {
        files: ['**/*.php']
      },
      options: {
        style: 'expanded',
        livereload : {
          port: 1337,
          host: 'publicknowledge.dev',
          key: grunt.file.read('/Users/rhea/Dropbox/_CLIENTS/sfmoma/public\ knowledge/SSL/publicknowledge.dev.key'),
          cert: grunt.file.read('/Users/rhea/Dropbox/_CLIENTS/sfmoma/public\ knowledge/SSL/publicknowledge.dev.crt'),
        },
      },
    },
  });

  
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['sass', 'concat', 'watch']);
 

};