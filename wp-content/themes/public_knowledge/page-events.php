<?php 
/**
 * Template Name: Events Page
 *
 */ 

get_header();

include(locate_template( 'modules/nav.php' ));

?>
<div class="mainBackgroundColor">
	<?php
		include(locate_template( 'modules/events.php' ));
		include(locate_template( 'modules/footer.php' ));
	?>
</div>
<?php get_footer(); ?>