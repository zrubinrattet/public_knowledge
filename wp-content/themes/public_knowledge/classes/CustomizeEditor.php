<?php 

class CustomizeEditor{
	public static function init(){
		add_shortcode( 'sfmomapkslider', 'CustomizeEditor::slider_handler' );
		add_shortcode( 'sfmomapkyoutube', 'CustomizeEditor::youtube_handler' );
		add_action( 'init', 'CustomizeEditor::register_slider_post_type' );

		add_filter( 'upload_mimes', 'CustomizeEditor::add_eps_support', 1, 1 );

	}
	public static function register_slider_post_type() {
	
		$labels = array(
			'name'                => __( 'Sliders', 'text-domain' ),
			'singular_name'       => __( 'Slider', 'text-domain' ),
			'add_new'             => _x( 'Add New Slider', 'text-domain', 'text-domain' ),
			'add_new_item'        => __( 'Add New Slider', 'text-domain' ),
			'edit_item'           => __( 'Edit Slider', 'text-domain' ),
			'new_item'            => __( 'New Slider', 'text-domain' ),
			'view_item'           => __( 'View Slider', 'text-domain' ),
			'search_items'        => __( 'Search Sliders', 'text-domain' ),
			'not_found'           => __( 'No Sliders found', 'text-domain' ),
			'not_found_in_trash'  => __( 'No Sliders found in Trash', 'text-domain' ),
			'parent_item_colon'   => __( 'Parent Slider:', 'text-domain' ),
			'menu_name'           => __( 'Sliders', 'text-domain' ),
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-images-alt',
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
			)
		);
	
		register_post_type( 'sfmomapk_slider', $args );
	}
	public static function slider_handler($atts, $content){
		$a = shortcode_atts( array(
			'id' => 1,
		), $atts );

		$output = '';
		$slides = get_field('slider_gallery', esc_attr($a['id']));
		if( !empty($slides) ):
			if(!empty(get_field('slider_header', esc_attr($a['id'])))){
				$output .= '<h2 class="sliderheader">' . get_field('slider_header', esc_attr($a['id'])) . '</h2>';
			}
			$output .= '<div class="slider">';
			foreach( $slides as $index => $slide ): 
				$output .= '<a href="' . $slide['url'] . '" class="slider-slide"';
				$output .= !empty($slide['caption']) ? ' data-caption="' . $slide['caption'] . '">' : '>';
				$output .= '<div class="slider-slide-image" style="background-image:url(\'' . $slide['sizes']['medium'] . '\');"></div>';
				$output .= '</a>';
			endforeach;

		$output .= '</div>';
		endif;

		return $output;
	}
	public static function add_eps_support($mimes){
		$mimes['eps'] = 'application/octet-stream';
		return $mimes;
	}
	
}

CustomizeEditor::init();

?>