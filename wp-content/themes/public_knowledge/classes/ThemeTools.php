<?php 
class ThemeTools{
	public static function get_site_bg_images(){
		$bg_urls = array();
		$sites = get_sites(array(
			'public' => 1,
			));
		foreach($sites as $site){
			switch_to_blog($site->blog_id);
			array_push($bg_urls, array(
				'image_url' => get_field('hero_bg_image', 'option'),
				'blog_id' => $site->blog_id,
				'sort_num' => get_field('hero_slider_order', 'option'),
				));
			restore_current_blog();
		}
		usort($bg_urls, function($a, $b){
			if( $a['sort_num'] < $b['sort_num'] ){
				return 1;
			}
			elseif( $a['sort_num'] == $b['sort_num'] ){
				return 0;
			}
			else{
				return -1;
			}
		});
		return $bg_urls;
	}
	// programatically add pages when a new blog is created
	public static function the_slug_exists($post_name, $blog_id = 1) {
		global $wpdb;
		$posts_table = $blog_id == 1 ? 'wp_posts' : 'wp_'.$blog_id.'_posts'; 
		if($wpdb->get_row("SELECT post_name FROM " . $posts_table . " WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) {
			return true;
		} else {
			return false;
		}
	}
	public static function recursive_empty($InputVariable) {
	   $Result = true;

	   if (is_array($InputVariable) && count($InputVariable) > 0) {
	      foreach ($InputVariable as $Value) {
	         $Result = $Result && ThemeTools::recursive_empty($Value);
	      }
	   }
	   else {
	      $Result = empty($InputVariable);
	   }

	   return $Result;
	}
	public static function get_image_for_opengraph() {
		global $post, $posts;

		ob_start();
		ob_end_clean();

		preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

		if( ThemeTools::recursive_empty($matches) ){ //Defines a default image
			return get_template_directory_uri() . '/library/img/logo-black.png';
		}
		else{
			return $matches[1][0];
		}
	}
	public static function get_image_for_bloggrid($size = 'full') {
		if (has_post_thumbnail()) {
			$image_id = get_post_thumbnail_id();
			$image_url = wp_get_attachment_image_src($image_id, $size);
			$image_url = $image_url[0];
		} 
		else {
			global $post, $posts;
			$image_url = '';
			ob_start();
			ob_end_clean();
			preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
			
			//Defines a default image
			if(ThemeTools::recursive_empty($matches)){
				$image_url = get_template_directory_uri() . "/library/img/placeholder.png";
			}
			else{
				$image_url = $matches [1] [0];
			}
		}
		return $image_url;
	}
	public static function the_bloggrid_item($post){
		?>
			<div class="bloggrid-item">
			<?php 

				$blog_to_switch_to = isset( $post->site_ID ) ? $post->site_ID : get_current_blog_id();
				$author_id = (int) $post->post_author;	
				$post_has_no_author = get_field('post_has_no_author', $post->ID);

				if( !$post_has_no_author ): ?>
					<div class="bloggrid-item-authorcontainer">
						<div class="bloggrid-item-authorcontainer-author">	
							<?php 
							$img_url;
							if( is_search() ) {
								switch_to_blog($blog_to_switch_to); 
								$field_value = get_field('user_profile_image_' . $blog_to_switch_to, 'user_' . $author_id);
								if( is_numeric($field_value) ){
									$img_url = wp_get_attachment_url(get_field('user_profile_image_' . $blog_to_switch_to, 'user_' . $author_id) );
								}
								else{
									$img_url = get_field('user_profile_image_' . $blog_to_switch_to, 'user_' . $author_id);
								}
							}
							else{
								$img_url = get_field('user_profile_image_' . $blog_to_switch_to, 'user_' . $author_id);
							}
								?>
							<?php 
							if( !empty( $img_url ) ): ?>
								<img src="<?php echo $img_url; ?>" class="bloggrid-item-authorcontainer-author-image">
							<?php 
								endif; 

								if( !is_search() ) switch_to_blog($blog_to_switch_to);
							if( !empty( get_the_author_meta( 'description', $post->post_author ) ) ):
							?>
								<div class="bloggrid-item-authorcontainer-author-description"<?php echo $blog_to_switch_to !== 1 ? ' style="border-top-color: ' . get_field('theme-color-tint', 'option') . ';"' : ''; ?>><?php echo apply_filters('the_content', get_the_author_meta( 'description', $post->post_author )); ?></div>
							<?php 
							endif; 
							?>
						</div>
					</div>
				<?php endif; ?>
				<div class="bloggrid-item-postcontainer<?php echo $post_has_no_author ? ' noauthor' : ''; ?>">
					<div class="bloggrid-item-postcontainer-post">
						<?php
							if( has_post_thumbnail( $post->ID ) && is_main_site( get_current_blog_id() ) ):
								$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' )[0];
								?>
								<img src="<?php echo $image; ?>" class="bloggrid-item-postcontainer-post-image">
							<?php
							endif;
						?>
						<a href="<?php the_permalink(); ?>" class="bloggrid-item-postcontainer-post-header"<?php echo $blog_to_switch_to !== 1 ? ' style="color: ' . get_field('theme-color-tint', 'option') . ';"' : ''; ?>><?php the_title(); ?></a>
						<h5 class="bloggrid-item-postcontainer-post-author">By <?php the_author(); ?></h5>
						<div class="bloggrid-item-postcontainer-post-social">
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink() ); ?>" target="_blank" class="bloggrid-item-postcontainer-post-social-link">
								<i class="bloggrid-item-postcontainer-post-social-link-icon sficon sficon-facebook"></i>
							</a>
							<a href="https://twitter.com/home?status=<?php echo urlencode( get_permalink() ); ?>" target="_blank" class="bloggrid-item-postcontainer-post-social-link">
								<i class="bloggrid-item-postcontainer-post-social-link-icon sficon sficon-twitter"></i>
							</a>
							<a href="#" target="_blank" class="bloggrid-item-postcontainer-post-social-link share" data-url="<?php the_permalink(); ?>">
								<i class="bloggrid-item-postcontainer-post-social-link-icon sficon sficon-share"></i>
							</a>
						</div>
						<div class="bloggrid-item-postcontainer-post-content">
							<?php echo !empty(get_the_excerpt()) ? apply_filters('the_content', get_the_excerpt()) : apply_filters('the_content', wp_trim_words( get_the_content(), 35, '' )); ?>
						</div>
						<a href="<?php the_permalink(); ?>" class="bloggrid-item-postcontainer-post-elipsis"<?php echo $blog_to_switch_to !== 1 ? ' style="color: ' . get_field('theme-color-tint', 'option') . ';"' : ''; ?>>&hellip;</a>
					</div>
				</div>
				<?php restore_current_blog(); ?>
			</div>
		<?php
	}
}
?>