<?php 
	
	/**
	* Events Manager Utility. Used to provide syntactic sweetness to working with events manager plugin
	*/
	class EMUtil{
		public function __construct(){
			// throw error if events manager isn't active
			if( !class_exists('EM_Events') ){
				echo new WP_Error('Undefined plugin Events Manager', 'Please activate the Events Manager plugin from the Wordpress admin before using this class.');
			}
		}
		public function get_events($args = array()){
			$defaults = array(
				'offset' => 0,
				'limit' => -1, // change this default if you want there to be some seemore in the future
				'orderby' => 'event_start_date',
				'archived' => false,
				'cat' => '',
			);

			$args = array_merge($defaults, $args);

			$limit = $args['limit'] == -1 ?  " LIMIT 18446744073709551615 " : " LIMIT " . $args['limit'];

			$cat = $args['cat'] == '' ? '' : "JOIN wp_term_relationships as tr 
						ON tr.object_id = ev.post_id
					JOIN wp_term_taxonomy as tt
						ON tt.term_taxonomy_id = tr.term_taxonomy_id
					JOIN wp_terms as t
						ON tt.term_id = t.term_id
						AND t.slug = '" . $args['cat'] . "'";

			$archived = $args['archived'] ? ' WHERE ev.event_end_date < CURDATE() ' : '  WHERE ev.event_end_date >= CURDATE() ';

			global $wpdb;
			$query_string = "SELECT * FROM " . $wpdb->prefix . "em_events as ev " . $cat . $archived . " AND ev.post_id in (SELECT ID from " . $wpdb->prefix . "posts WHERE post_status = 'publish') ORDER BY ev." . $args['orderby'] . $limit . " OFFSET " . $args['offset'];

			return $wpdb->get_results($query_string, ARRAY_A);
		}
		public function get_event($post_id){
			global $wpdb;
			return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "em_events WHERE post_id = " . (string) $post_id, ARRAY_A);
		}
		public function get_event_location($post_id){
			global $wpdb;

			return $wpdb->get_results("SELECT location_id, post_id, location_slug, location_name, location_owner, location_address, location_town, location_state, location_postcode, location_country, location_status, location_private FROM " . $wpdb->prefix . "em_locations where location_id = (select meta_value from wp_postmeta where post_id = " . $post_id . " and meta_key = '_location_id')", ARRAY_A)[0];
		}
		public function get_excerpt($post_id){
			return get_post_field('post_excerpt', $post_id );
		}
		public function get_event_category_list($post_id){
			return wp_get_object_terms( $post_id, 'event-categories' );
		}
		public function render_event_category_list_as_links($term_list){
			foreach( $term_list as $term ):
			?>
				<a href="<?php echo get_term_link( $term, 'event-categories' ); ?>"><?php echo $term->name ?></a>
			<?php
			endforeach;
		}
		public function get_ical_url($post_id){
			return get_permalink( $post_id ) . 'ical/';
		}
		public function get_featured_image($post_id){
			return get_the_post_thumbnail_url($post_id, 'full');
		}
		public function get_content($post_id){
			return get_post_field('post_content', $post_id);
		}
		public function get_timerange($start, $end){
			if( $start === '00:00:00' && $end === '23:59:59' ){
				return 'All day';
			}
			else{
				$times = [];
				foreach( [$start, $end] as $time ){
					$times[] = $this->do_timestr($time);
				}
				return implode('-', $times);
			}
		}
		private function do_timestr($time){
			// build array hours:minutes:seconds
			$time_arr = explode(':', $time);
			// set ampm
			if( intval($time_arr[0]) - 12 >= 0 ){
				$is_pm = true;
			}
			else{
				$is_pm = false;
			}
			// get hours
			$time_str = (string) intval($time_arr[0]) >= 13 ? intval($time_arr[0]) - 12 : ($time_arr[0] == '00' ? '12' : $time_arr[0]);
			// trim zero from beginning
			$time_str = intval($time_str);
			// add minutes
			$time_str .= $time_arr[1] == '00' ? '' : ':' . $time_arr[1];
			// add ampm
			$time_str .= $is_pm ? 'PM' : 'AM';
			return $time_str;
		}
		public function get_start_day($start_date){
			return date('M j', strtotime($start_date));
		}
		public function get_start_time($start_time){
			return $this->do_timestr($start_time);
		}
		public function get_datetimerange($post_id){
			// entry var
			$return = '';

			// build datetimes
			$event_start = new DateTime(get_post_meta($post_id, '_event_start', true));
			$event_end = new DateTime(get_post_meta($post_id, '_event_end', true));
			$timehtml = '';
			$start_time = '';
			$end_time = '';

			// is all day?
			if( $event_start->format('His') == '000000' && $event_end->format('His') == '235959' ){
				$timehtml = 'All day';
			}
			else{
				// start time has no minutes?
				if( $event_start->format('i') == '00' ){
					$start_time = $event_start->format('g');
				}
				else{
					$start_time = $event_start->format('g:i');
				}
				
				// end time has no minutes?
				if( $event_end->format('i') == '00' ){
					$end_time = $event_end->format('g');
				}
				else{
					$end_time = $event_end->format('g:i');
				}

				// same AM/PM?
				if( $event_start->format('a') == $event_end->format('a') ){
					$timehtml .= $start_time . '-' . $end_time . $event_end->format('A');
				}
				else{
					$timehtml .= $start_time . $event_start->format('A') . '-' . $end_time . $event_end->format('A');
				}
			}

			// is same day?
			if( $event_start->format('Y-m-d') == $event_end->format('Y-m-d') ){
				$datehtml = $event_start->format('F jS, Y');
				if( $timehtml != 'All day' ){
					$timehtml = $timehtml;
				}
				$return .= $datehtml;
				$return .= ' ';
				$return .= $timehtml;
			}
			else{
				// define vars
				$year = '';
				$month = '';
				$datehtml = $event_start->format('F jS');

				// is same year?
				if( $event_start->format('Y') == $event_end->format('Y') ){
					$year = $event_end->format('Y');
				}
				// is same month?
				if( $event_start->format('F') == $event_end->format('F') ){
					$month = $event_end->format('F');
				}

				// build datehtml
				// 
				// is same year?
				if( !empty($year) ){
					// is same month?
					if( !empty($month) ){
						$datehtml .= '-' . $event_end->format('jS, Y');
					}
					else{
						$datehtml .= '-' . $event_end->format('F jS, Y');
					}
				}
				// is same month & different year?
				elseif( !empty($month) ){
					$datehtml .= $event_start->format(' Y') . '-' . $event_end->format('F jS, Y');
				}
				// is not same month day or year
				else{
					$datehtml .= $event_start->format(' Y') . '-' . $event_end->format('F jS, Y');
				}

				$return .= $datehtml;
				$return .= ' ';
				$return .= 'Daily, ' . $timehtml;
			}
			return $return;
		}
		public function get_event_html($event){

			$output = '';
			$output .= '<div class="events-grid-item">';
			$output .= '<div class="events-grid-item-datetime">';
			$output .= '<div class="events-grid-item-datetime-badge">';
			$output .= '<div class="events-grid-item-datetime-badge-startday">' . $this->get_start_day($event['event_start_date']) . '</div>';
			$output .= '<div class="events-grid-item-datetime-badge-starttime">' . $this->get_start_time($event['event_start_time']) .'</div>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '<div class="events-grid-item-content">';
			$output .= '<a href="' . get_permalink($event['post_id']) . '" class="events-grid-item-content-title">' . $event['event_name'] . '</a>';
			$cats = $this->get_event_category_list($event['post_id']);
			if( !empty($cats) ){
				$output .= '<div class="events-grid-item-content-cats">';
				foreach( $cats as $cat ){
					$output .= '<a class="events-grid-item-content-cats-cat" href="' . get_term_link( $cat->term_id, 'event-categories' ) . '">' . $cat->name . '</a> ';
				}
				$output .= '</div>';
			}
			$location = $this->get_event_location($event['post_id']);	
			$location_string = '';
			if( !empty($location) ){
				$location_string = $location['location_name'] . '<br/>' . $location['location_address'] . ', ' . $location['location_town'] . ', ' . $location['location_state'] . ' ' . $location['location_postcode'];
			}

			$output .= empty($location_string) ? '<div class="events-grid-item-content-address">This event has no location</div>' : '<a class="events-grid-item-content-address" href="' . get_permalink( $location['post_id'] ) . '">' . $location_string . '</a>';
		
			$output .= '<div class="events-grid-item-content-timerange">';
			$output .= $this->get_datetimerange( $event['post_id'] );
			$output .= '</div>';
			$notes = get_post_meta( $event['post_id'], 'Notes', true );
			if( !empty($notes) ):
				$output .= '<div class="events-grid-item-content-notes">' . $notes . '</div>';
			endif;
			$facebook_event_url = get_post_meta($event['post_id'], 'Facebook Event URL', true);
			if( !empty($facebook_event_url) ):
				$output .= '<a href="' . $facebook_event_url . '" class="events-grid-item-content-facebook">RSVP on Facebook</a>';
			endif;
			$ical_url = $this->get_ical_url( $event['post_id'] );
			if( !empty($ical_url) ):
				$output .= '<a href="' . $ical_url . '" class="events-grid-item-content-ical">Add to iCal/Google</a>';
			endif; 

			if( has_post_thumbnail( $event['post_id'] ) ):
				$output .= '<img src="' . $this->get_featured_image($event['post_id']) . '" class="events-grid-item-content-featuredimage">';
			endif; 
			$excerpt = $this->get_excerpt( $event['post_id'] );
			if( !empty($excerpt) ):
				$output .= '<div class="events-grid-item-content-content">' . $excerpt . '</div>';
			endif;

			$output .= '</div>';
			$output .= '</div>';

			return $output;
		}
	}

?>