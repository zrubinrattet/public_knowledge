<?php 
class CustomizeFrontend{
	public static function init(){
		add_filter( 'excerpt_length', 'CustomizeFrontend::custom_excerpt_length', 999 );
		add_filter( 'excerpt_more', 'CustomizeFrontend::custom_excerpt_more' );
		add_filter( 'get_search_form', 'CustomizeFrontend::modify_search_form', 1, 10 );

		add_action( 'wp_ajax_seemore', 'CustomizeFrontend::handle_seemore' );
	}
	public static function custom_excerpt_more($more){
		return '...';
	}
	public static function custom_excerpt_length($length){
		return 20;
	}
	public static function modify_search_form($form){
		return '<form role="search" method="get" class="searchform" action="' . esc_url( site_url( ) ) . '">
					<div class="searchform-wrapper">
						    <input type="search" class="searchform-wrapper-input" placeholder="' . esc_attr_x( 'Search&hellip;', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" />
							<input class="searchform-wrapper-checkbox" type="checkbox" name="s_proj"><span class="searchform-wrapper-text">Search this project</span>	
					</div>
					<div class="searchform-closewrapper">
						<i class="searchform-closewrapper-close sficon sficon-close"></i>
					</div>
	            	<div class="searchform-tint"></div>		
	            </form>';
	}
	public static function handle_seemore(){
		$args = array(
			'posts_per_page' => 10,
			'offset' => $_POST['offset'],
		);

		if( isset($_POST['term_id']) && isset($_POST['taxonomy']) ){
			$args['tax_query'] = array(
				array(
					'taxonomy' => $_POST['taxonomy'],
					'field' => 'id',
					'terms' => $_POST['term_id'],
				)
			);
		}

		$the_query = new WP_Query($args);

		ob_start();
		foreach( $the_query->posts as $post ){
			$the_query->the_post();
			ThemeTools::the_bloggrid_item($post);
		}
		echo json_encode(array(
			'data' => ob_get_clean(),
			'totalposts' => $the_query->found_posts,
		));
		wp_die();
	}
}

CustomizeFrontend::init();

?>