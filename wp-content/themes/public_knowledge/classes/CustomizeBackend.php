<?php 

require('PHPImage.php');

class CustomizeBackend{
	public static function init(){
		add_action('admin_head', 'CustomizeBackend::hide_gravatar_from_profile_backend' );
		add_action('wpmu_new_blog', 'CustomizeBackend::set_default_theme', 10, 6);
		add_action('wpmu_new_blog', 'CustomizeBackend::deactivate_site', 10, 6);
		add_action('wpmu_new_blog', 'CustomizeBackend::build_site_pages');
		add_action('wpmu_new_blog', 'CustomizeBackend::build_inpagenav_nav', 20, 6);
		add_action('init', 'CustomizeBackend::remove_page_editor');
		add_action('acf/save_post', 'CustomizeBackend::update_logo_text_image');
		add_filter('ms_site_check','CustomizeBackend::redirect_hidden_sites');
	}
	public static function build_inpagenav_nav( $blog_id, $user_id, $domain, $path, $site_id, $meta ){
		switch_to_blog($blog_id);

		$name = 'In-Page Nav';
        $menu_id = wp_create_nav_menu($name);
        $menu = get_term_by( 'name', $name, 'nav_menu' );

		$pages = array(
			array(
				'menu-item-title' => 'Overview',
				'menu-item-object-id' => get_page_by_path('about')->ID,
				'menu-item-db-id' => 0,
				'menu-item-object' => 'page',
				'menu-item-parent-id' => 0,
				'menu-item-type' => 'post_type',
				'menu-item-url' => site_url('/about/'),
				'menu-item-status' => 'publish',
			),
			array(
				'menu-item-title' => 'Blog',
				'menu-item-object-id' => get_page_by_path('blog')->ID,
				'menu-item-db-id' => 0,
				'menu-item-object' => 'page',
				'menu-item-parent-id' => 0,
				'menu-item-type' => 'post_type',
				'menu-item-url' => site_url('/blog/'),
				'menu-item-status' => 'publish',
			),
		);

		foreach($pages as $page){
			wp_update_nav_menu_item( $menu->term_id, 0, $page );
		}

		// theme_set_nav_menu($menu->term_id, 'inpagenav-nav');

		$locations = get_theme_mod('nav_menu_locations'); //get the menu locations
	    $locations['inpage-nav'] = $menu_id; //set our new menu to be the main nav
	    set_theme_mod('nav_menu_locations', $locations); //update 

		restore_current_blog();
	}
	public static function deactivate_site( $blog_id, $user_id, $domain, $path, $site_id, $meta ){
		update_blog_status($blog_id, 'archived', '1');
	}
	public static function redirect_hidden_sites() {
	    // Super Admins/Admins always get in
	    if ( is_super_admin() || is_admin() || current_user_can( 'manage_options' ) ) {
	        return true;
	    } 
	    else {
	        // Defines
	        if ( defined( 'NOBLOGREDIRECT' ) ) {
	            $goto = NOBLOGREDIRECT;
	        } else {
	            $goto = network_site_url();
	        }
	 
	        $blog = get_blog_details();
	 
	        if( '1' == $blog->deleted || '2' == $blog->deleted || '1' == $blog->archived || '1' == $blog->spam ) {
	            wp_redirect( $goto );
	            die();
	        }
	    }
	}
	public static function hide_gravatar_from_profile_backend() {
	    $screen = get_current_screen();
	    if ( $screen->id != "profile" && $screen->id != 'profile-network' && $screen->id != "user-edit" ){
	        return;
	    }
	    ?>
	    <style>
	    	.user-profile-picture{
	    		display: none;
		    }
	    </style>
	    <?php
	}
	public static function update_logo_text_image(){
		$screen = get_current_screen();
		// if( basename($_SERVER['REQUEST_URI']) == 'admin.php?page=general-settings' ){
		if (strpos($screen->id, 'general-settings') == true && get_current_blog_id() != 1) {

			// delete old picture if it exists & is writable
			$old_image = realpath(get_template_directory() . '/library/img/project-' . (string) get_current_blog_id() . '-' . get_blog_option(get_current_blog_id(), 'current_project_texthero_hash') . '-text.png');
			if(file_exists($old_image) && is_writable($old_image)){
				unlink($old_image);
			}	

			$bg = get_template_directory() . '/library/img/projecttitle-canvas.png';

			switch_to_blog(get_current_blog_id());

			$phpimg = new PHPImage();

			$phpimg->setDimensionsFromImage($bg);
			$phpimg->setQuality(9);
			$phpimg->setFont(get_template_directory() . '/library/fonts/SFMOMADisplayHeavy.ttf');

			$phpimg->setTextColor(!empty(get_field('theme-color-tint-toggle', 'option')) ? ColorBeast::hex_to_rgb(get_field('theme-color-tint', 'option')) : array(255,255,255));

			$phpimg->text(get_bloginfo('name'), array(
			        'fontSize' => 260, 
			        'x' => 500,
			        'y' => 0,
			        'width' => 2840,
			        'height' => 2160,
			        'alignHorizontal' => 'center',
			        'alignVertical' => 'center',
			    ));

			$phpimg->setOutput('png');

			//begin hash
			$hash = hash('crc32', rand());
			// update wp_blog_option table
			update_blog_option(get_current_blog_id(), 'current_project_texthero_hash', $hash);

			$phpimg->save(get_template_directory() . '/library/img/project-' . (string) get_current_blog_id() . '-' . $hash . '-text.png');


			//////////////////
			// INVERT ALPHA //
			//////////////////

			// set up source
			$source = imagecreatefrompng(get_template_directory() . '/library/img/project-' . (string) get_current_blog_id() . '-' . $hash . '-text.png');
			
			// set up dest
			$dest = imagecreatetruecolor(imagesx($source),imagesy($source));
			imagealphablending($dest,false);
			imagesavealpha($dest,true);

			// get rgb values of tint
			$tint_rgb = !empty(get_field('theme-color-tint-toggle', 'option')) ? ColorBeast::hex_to_rgb(get_field('theme-color-tint', 'option')) : array(255,255,255);
			$r = (int) $tint_rgb[0];
			$g = (int) $tint_rgb[1];
			$b = (int) $tint_rgb[2];

			// copy source to dest
			$trans = imagecolorallocatealpha($dest,$r,$g,$b,127);
			for ($y=0;$y<imagesy($dest);$y++) {
				for ($x=0;$x<imagesx($dest);$x++) {
					imagesetpixel($dest,$x,$y,$trans);
				}
			}
		 	
		 	// invert alpha of dest
			for ($y=0;$y<imagesy($source);$y++) {
				for ($x=0;$x<imagesx($source);$x++) {
					$rgb = imagecolorat($source,$x,$y);
					$a = ($rgb >> 24) & 0xFF;
					$a = $a == 127 ? 0 : 127;
					$color = imagecolorallocatealpha($dest,$r,$g,$b,$a);
					imagesetpixel($dest,$x,$y,$color);
				}
			}

			// save
			imagepng($dest, get_template_directory() . '/library/img/project-' . (string) get_current_blog_id() . '-' . $hash . '-text.png');

			// cleanup
			restore_current_blog();

		}

	}
	public static function set_default_theme( $blog_id, $user_id, $domain, $path, $site_id, $meta ){
		update_blog_option($blog_id, 'template', 'public_knowledge' );
		update_blog_option($blog_id, 'stylesheet', 'public_knowledge' );
	}
	public static function build_site_pages($blog_id){
		// register pages
		$pages = array(
			array(
				'title' => 'Blog',
				'slug' => 'blog',
			),
			array(
				'title' => 'About',
				'slug' => 'about',
			),
			array(
				'title' => "Docs",
				'slug' => 'docs',
			),
			array(
				'title' => 'Projects',
				'slug' => 'projects',
			),
		);
		switch_to_blog($blog_id);

		// add all the pages
		foreach ($pages as $page){
			if(!ThemeTools::the_slug_exists($page['slug'], $blog_id)){
			    $page_title = $page['title'];
			    $page_check = get_page_by_title($page_title);
			    $page_args = array(
				    'post_type' => 'page',
				    'post_title' => $page_title,
				    'post_status' => 'publish',
				    'post_author' => 1,
				    'post_slug' => $page['slug'],
				    'page_template' => 'page-' . $page['slug'] . '.php',
			    );
			    if(!isset($page_check->ID) && !ThemeTools::the_slug_exists($page['slug'], $blog_id)){
			    	// only add docs, projects & calendar pages to main site
			    	if( !is_main_site( $blog_id ) && ( $page['slug'] == 'projects' || $page['slug'] == 'docs' ) ){

			    	}
			    	else{
			    		// change blog page to "The Stacks" for the main site
			    		if( is_main_site() && $page['title'] == 'Blog' ){
			    			$page['title'] = 'The Stacks';
			    			$page['slug'] = 'the-stacks';
			    		}
			    		wp_insert_post($page_args);	
			    	}
			    }	
			}
		}
		// delete sample page
		$sample_page = get_page_by_title('Sample Page');
		wp_delete_post( $sample_page->ID );

		// delete hello world post
		$hello_world = get_page_by_path( 'hello-world', OBJECT, 'post' );
		wp_delete_post( $hello_world->ID );

		// set permalink structure to site_url/postname
		global $wp_rewrite;
		$wp_rewrite->set_permalink_structure( '/%postname%/' );
		$wp_rewrite->flush_rules();

		restore_current_blog();
	}
	public static function remove_page_editor(){
		if( !is_admin() ){
			return;
		}

		// Get the post ID on edit post with filter_input super global inspection.
	    $current_post_id = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );
	    // Get the post ID on update post with filter_input super global inspection.
	    $update_post_id = filter_input( INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT );

	    // Check to see if the post ID is set, else return.
	    if ( isset( $current_post_id ) ) {
	       $post_id = absint( $current_post_id );
	    } 
	    else if ( isset( $update_post_id ) ) {
	       $post_id = absint( $update_post_id );
	    } 
	    else {
	       return;
	    }
	    if ( isset( $post_id ) ) {
	    	$post_template = get_post_meta($post_id, '_wp_page_template', true);

	    	add_post_type_support( 'page', 'editor' );	


	    	if($post_template != 'page-about.php' && $post_template != 'page-raw.php' && $post_template != 'page-general.php' && $post_template != 'page-contact.php'){
		    	remove_post_type_support( 'page', 'editor' );
	    	}

	    	remove_post_type_support( 'page', 'thumbnail' );
	    }
	}
}

CustomizeBackend::init();
?>