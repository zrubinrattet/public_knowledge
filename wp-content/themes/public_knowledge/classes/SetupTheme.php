<?php 
class SetupTheme {
	public static function init(){
		show_admin_bar( false );
		add_filter( 'body_class', 'SetupTheme::add_slug_to_body_class' );
		add_action( 'after_setup_theme', function(){
			add_theme_support( 'html5' );
			add_theme_support( 'post-thumbnails' );
		});

		add_action( 'init', function(){
			SetupTheme::clean_head();
			register_nav_menu( 'in-page-nav', 'In-Page Navigation');
		});

		add_action( 'wp_enqueue_scripts', function(){
			SetupTheme::register_styles();
			SetupTheme::enqueue_styles();
			SetupTheme::register_javascript();
			SetupTheme::enqueue_javascript();	
			SetupTheme::localize_scripts();	
		});
	}

	public static function enqueue_javascript(){
		wp_enqueue_script( 'theme' );
	}
	public static function enqueue_styles(){
		wp_enqueue_style( 'theme' );
	}

	public static function register_javascript(){
		wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js');
	}

	public static function register_styles(){
		wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css' );
	}

	public static function localize_scripts(){
		if( get_current_blog_id() == 1 ){

			$calendar_page = get_posts(array(
				'post_type' => 'page',
			    'fields' => 'ids',
			    'nopaging' => true,
			    'meta_key' => '_wp_page_template',
			    'meta_value' => 'page-calendar.php'
			));
			$cal_info = array(
				'key' => strval(get_field('google_api_key', $calendar_page[0])),
				'id' => strval(get_field('google_calendar_id', $calendar_page[0])),
			);
			wp_localize_script( 'theme', 'GoogleCal', $cal_info );

		}
		
		wp_localize_script( 'theme', 'ProjectUrls', array(
			'main' => network_site_url(),
			'current' => site_url(),
		) );

		wp_localize_script( 'theme', 'AJAXURL', admin_url( 'admin-ajax.php' ));

		if( is_main_site(get_current_blog_id()) ){
			$emutil = new EMUtil();

			$events = $emutil->get_events(array(
				'limit' => -1,
				'archived' => isset($_GET['archived']),
			));
			
			wp_localize_script( 'theme', 'EMPOSTS', strval(count($events)) );
		}

	}
	public static function add_slug_to_body_class($classes){
	    // When using https://developer.wordpress.org/reference/functions/body_class the current page slug gets added
	    global $post;
	    if (is_home()) {
	        $key = array_search('blog', $classes);
	        if ($key > -1) {
	            unset($classes[$key]);
	        }
	    } elseif (is_page()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    } elseif (is_singular()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    }
	    
	    return $classes;
	}
	public static function clean_head(){
		// removes generator tag
		remove_action( 'wp_head' , 'wp_generator' );
		// removes dns pre-fetch
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		// removes weblog client link
		remove_action( 'wp_head', 'rsd_link' );
		// removes windows live writer manifest link
		remove_action( 'wp_head', 'wlwmanifest_link');	
	}
}
SetupTheme::init();
?>