<?php 
	
	class EMSeemore{
		public static function init(){
			add_action('wp_ajax_nopriv_emseemore', 'EMSeemore::ajax_handler');
			add_action('wp_ajax_emseemore', 'EMSeemore::ajax_handler');
		}
		public function ajax_handler(){
			$emutil = new EMUtil();

			$args = isset($_GET['archived']) ? array('archived' => true, 'offset' => $_POST['offset']) : array('offset' => $_POST['offset']);

			$events = $emutil->get_events($args);

			$output = '';

			foreach($events as $event){
				$output .= $emutil->get_event_html($event);
			}
			
			echo $output;

			wp_die();
		}
	}

	EMSeemore::init();
?>