<?php 
	/**
	* Handles the Emails coming in from the contact form on the contact page
	*/
	class EmailHandler {
		public static function _init(){
			add_action( 'wp_ajax_handle_email_submit', 'EmailHandler::handle_email_submit' );
			add_action( 'wp_ajax_nopriv_handle_email_submit', 'EmailHandler::handle_email_submit' );
		}
		public static function handle_email_submit(){
			$email = sanitize_email( $_POST['email'] );

			// validated on the front end but that can be changed easily
			// checking if a value is submitted
			if( empty($email) ){
				echo json_encode(array(
					'error' => 'Please enter your email address.'
				));
				wp_die();
			}

			// check if it's an email
			if( is_email($email) ){
				if( wp_mail( get_option('admin_email'), 'New Submission from Public Knowledge contact form', $email ) ){
					echo 'Mail sent successfully';
				}
				else{
					echo json_encode(array(
						'error' => 'Email address not sent.'
					));
				}
				wp_die();
			}
			else{
				echo json_encode(array(
					'error' => 'Email address not valid.'
				));
				wp_die();
			}

			wp_die();
		}
	}
	EmailHandler::_init();
?>