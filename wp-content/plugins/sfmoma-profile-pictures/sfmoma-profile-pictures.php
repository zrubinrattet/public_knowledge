<?php 
	
	/**
	 * @package SFMOMAProfilePictures
	 */
	/*
	Plugin Name: SFMOMA Profile Pictures
	Description: Adds an image field to edit profile pages. Compatible with multisite. Requires ACF Pro.
	Version: 0.0a
	Author: Zach Rubin-Rattet
	Author URI: www.sfmoma.org
	License:
	*/

	// no direct access
	defined( 'ABSPATH' ) or die('Nope ><');

	class SFMOMA_PP{
		public function __construct(){
			add_action( 'acf/init', array($this, 'add_fields') );
		}
		public function add_fields(){
			if( function_exists('acf_add_local_field_group') ){
				acf_add_local_field_group(array(
					'key' => 'group_3398hoifasdad',
					'title' => 'Profile Image',
					'fields' => array(
						array(
							'key' => 'field_nnh32812',
							'label' => 'Profile Image',
							'name' => 'user_profile_image',
							'type' => 'image',
							'return_format' => 'url',
							'instructions' => 'Use this instead of gravatar',
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'user_role',
								'operator' => '==',
								'value' => 'all',
							),
						),
					),
				));
			}
		}
	}

	new SFMOMA_PP();

?>