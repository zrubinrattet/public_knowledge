;(function ( $, window, document, undefined ) {

	$(document).ready(function(){

		var UpdateEmailHandler = {
			container : $('.updateemail'),
			_init : function(){
				// if we're on the admin page
				if( UpdateEmailHandler.container.length > 0 ){

					// set additional vars
					UpdateEmailHandler.input = $('input[type="text"]', UpdateEmailHandler.container);
					UpdateEmailHandler.submit = $('input[type="button"]', UpdateEmailHandler.container);
					UpdateEmailHandler.messages = $('.updateemail-messages', UpdateEmailHandler.container);

					// update input placeholder
					if( typeof notify_email == 'string' && notify_email.length > 0 ){
						UpdateEmailHandler.input.attr('placeholder', notify_email);
					}

					// click handler
					UpdateEmailHandler.submit.on('click', UpdateEmailHandler._submitHandler);
					UpdateEmailHandler.input.on('keydown', UpdateEmailHandler._submitHandler);
				}
			},
			_submitHandler : function(e){
				// if enter key has been pressed from input or submit button has been clicked
				if( (e.type == 'keydown' && e.keyCode == 13) || e.type == 'click' ){
					// if email is valid
					if( UpdateEmailHandler._isEmail(UpdateEmailHandler.input.val()) ){

						// award
						UpdateEmailHandler.input.removeClass('updateemail--right');
						UpdateEmailHandler.input.addClass('updateemail--right');
						setTimeout(function(){
							UpdateEmailHandler.input.removeClass('updateemail--right');	
						}, 300);

						// add updating notice
						UpdateEmailHandler.messages.empty();
						UpdateEmailHandler.messages.prepend('<img class="updateemail-messages-spinner" src="' + adminurl + 'images/spinner.gif"/> Sending...');

						// ajax
						$.post(ajaxurl, {
							action : 'sfmoma_ns_update_email',
							email : UpdateEmailHandler.input.val(),
						}, function(response){
							try {
								response = JSON.parse(response);
								// success
								if( response ){
									UpdateEmailHandler.messages.empty();
									UpdateEmailHandler.messages.prepend('Success!');
									UpdateEmailHandler.input.attr('placeholder', UpdateEmailHandler.input.val());
									UpdateEmailHandler.input.val('');
								}
								// fail
								else{
									UpdateEmailHandler.messages.empty();
									UpdateEmailHandler.messages.prepend('An error occurred. Please see console for errors, refresh & try again.');
								}
							} catch(e) {
								// statements
								console.log(e);
							}
						});
					}
					else{
						// warn
						UpdateEmailHandler.input.removeClass('updateemail--wrong');
						UpdateEmailHandler.input.addClass('updateemail--wrong');
						setTimeout(function(){
							UpdateEmailHandler.input.removeClass('updateemail--wrong');	
						}, 300);
					}
				}
			},
			_isEmail : function(email) {
			  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  return regex.test(email);
			}
		}

		UpdateEmailHandler._init();
		
	});

})( jQuery, window, document );