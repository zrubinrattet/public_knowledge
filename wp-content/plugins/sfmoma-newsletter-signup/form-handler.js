;(function ( $, window, document, undefined ) {

	$(document).ready(function(){

		var FormHandler = {
			_init : function(){
				if( $('#sfmoma_ns-form').length > 0 ){
					FormHandler.messages = $('#sfmoma_ns-messages');
					FormHandler.form = $('#sfmoma_ns-form');
					FormHandler.nonce = $('#sfmoma_ns-nonce');
					FormHandler.name = $('#sfmoma_ns-name');
					FormHandler.email = $('#sfmoma_ns-email');
					FormHandler.submit = $('#sfmoma_ns-submit');
					FormHandler.submit.on('click', FormHandler._clickHandler);
				}		
			},
			_clickHandler : function(e){
				e.preventDefault();

				FormHandler.messages.empty();
				// if invalid email or name is blank
				if( !FormHandler._isEmail( FormHandler.email.val() ) || FormHandler.name.val().replace(' ', '') == '' ){
					if( !FormHandler._isEmail( FormHandler.email.val() )){
						FormHandler.messages.prepend('<div class="sfmoma_ns-messages-message error">Your email is invalid/required.</div>');
					}
					if( FormHandler.name.val().replace(' ', '') == '' ){
						FormHandler.messages.prepend('<div class="sfmoma_ns-messages-message error">Your full name cannot be blank.</div>');
					}
				}
				else{
					FormHandler.form.hide();
					var data = {
						action : 'sfmoma_ns_handle_form',
						nonce_val : FormHandler.nonce.val(),
						name : FormHandler.name.val(),
						email : FormHandler.email.val(),
					};
					$.post(ajaxurl, data, function(response){
						FormHandler.messages.prepend('<div class="sfmoma_ns-messages-message response">' + response + '</div>');
					});
				}
			},
			_isEmail : function(email) {
			  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  return regex.test(email);
			}
		};

		FormHandler._init();

	});

})( jQuery, window, document );