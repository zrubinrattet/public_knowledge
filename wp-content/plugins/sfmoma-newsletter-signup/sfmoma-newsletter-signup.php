<?php 
	
	/**
	 * @package SFMOMANewsletterSignup
	 */
	/*
	Plugin Name: SFMOMA Newsletter Signup
	Description: Add newsletter signup functionality via shortcode & db collection.
	Version: 0.0a
	Author: Zach Rubin-Rattet
	Author URI: www.sfmoma.org
	License:
	*/

	// no direct access
	defined( 'ABSPATH' ) or die('Nope ><');

	class SFMOMA_NS{
		public static function init(){

			// define plugin_dir & plugin_dir_url consts
			if( !defined('PLUGIN_DIR') ){
			    define('PLUGIN_DIR', ABSPATH . 'wp-content/plugins/sfmoma-newsletter-signup');
			}
			if( !defined('PLUGIN_DIR_URL') ){
			    define('PLUGIN_DIR_URL', plugin_dir_url(__FILE__));
			}

			// build menu page/add scripts
			add_action( 'network_admin_menu', 'SFMOMA_NS::add_menu_page' );
			add_action( 'wp_enqueue_scripts', 'SFMOMA_NS::enqueue_fe_scripts' );
			add_action( 'admin_enqueue_scripts', 'SFMOMA_NS::enqueue_be_scripts' );

			// create tables on activation
			register_activation_hook( __FILE__, 'SFMOMA_NS::create_tables' );

			// handle ajax submitting FE form
			add_action( 'wp_ajax_sfmoma_ns_handle_form', 'SFMOMA_NS::on_form_submit' );
			add_action( 'wp_ajax_nopriv_sfmoma_ns_handle_form', 'SFMOMA_NS::on_form_submit' );
			
			// handle ajax submitting email update BE form
			add_action( 'wp_ajax_sfmoma_ns_update_email', 'SFMOMA_NS::on_update_email' );
		}
		public static function enqueue_fe_scripts(){
			wp_enqueue_script( 'sfmoma-newsletter-signups', PLUGIN_DIR_URL . 'form-handler.js', array('jquery'), null, true );
			wp_localize_script( 'sfmoma-newsletter-signups', 'ajaxurl', admin_url( 'admin-ajax.php' ) );	
		}
		public static function enqueue_be_scripts(){
			wp_enqueue_script( 'sfmoma-newsletter-signups', PLUGIN_DIR_URL . 'update-email-handler.js', array('jquery'), null, true );
			wp_localize_script( 'sfmoma-newsletter-signups', 'adminurl', admin_url() );
			wp_localize_script( 'sfmoma-newsletter-signups', 'notify_email', get_site_option( 'sfmoma_ns_notify-email' ) );	
		}
		public static function add_menu_page(){
			add_menu_page( 'Newsletter Signups', 'Newsletter Signups', 'create_sites', 'sfmoma-newsletter-signups', 'SFMOMA_NS::build_menu_page', 'dashicons-welcome-widgets-menus', 90 );
		}
		public static function build_menu_page(){
			?>
			<div class="wrap">
				<h1 style="display: inline-block; vertical-align: middle;">Newsletter Signups</h1>
				<?php 
					global $wpdb;
					$results = $wpdb->get_results("SELECT time, name, email from {$wpdb->prefix}sfmoma_newsletter_signups order by time desc", ARRAY_A);
					if( !empty($results) ):
				?>
					<style>
						table {
						    font-family: arial, sans-serif;
						    border-collapse: collapse;
						    width: 100%;
						    background-color: white;
						}

						td, th {
						    border: 1px solid #dddddd;
						    text-align: left;
						    padding: 8px;
						}

						tr:nth-child(even) {
						    background-color: #f6f6f6f6;
						}
						.updateemail{
							display: inline-block;
							position: relative;
							top: 10px;
							left: 40px;
						}
						.updateemail input[type="text"]{
							border: 2px solid transparent;
							transition: 1s ease border-color !important;
						}
						.updateemail--wrong{
							border-color: red !important;
						}
						.updateemail--right{
							border-color: green !important;
						}
						.updateemail-messages-spinner{
							position: relative;
							top: 5px;
						}
					</style>
					
					
					<a href="<?php echo PLUGIN_DIR_URL . 'export' . get_site_option('sfmoma_ns_export-hash') . '.csv'; ?>" class="button button-secondary" style="margin: 10px 0px 0px 0px;">Export CSV</a>
					<div class="updateemail">
						<span> Update Notification Email Address: </span>	
						<input type="text" name="email" placeholder=""/>
						<input type="button" name="submit" value="Update" class="button button-primary">
						<span class="updateemail-messages"></span>
					</div>
					<table style="margin-top: 10px;">
						<tr>
							<th>Date/Time</th>
							<th>Name</th>
							<th>Email</th>
						</tr>
						<?php foreach( $results as $result ): ?>
							<tr>
								<td><?php echo date('D, M jS Y \a\t g:ia', strtotime($result['time'])); ?></td>
								<td><?php echo $result['name']; ?></td>
								<td><?php echo $result['email']; ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				<?php else: ?>
					<div>There's no signups :(</div>
				<?php endif; ?>
			</div>
			<?php
		}
		public static function create_tables(){
			global $wpdb;

			$table_name = $wpdb->prefix . 'sfmoma_newsletter_signups';
			
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE IF NOT EXISTS $table_name (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				name text NOT NULL,
				email text NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
		public static function render_signup_form($formclass = '', $fieldclass = ''){
			?>	
				<div id="sfmoma_ns-messages">
					
				</div>
				<form id="sfmoma_ns-form"<?php echo empty($formclass) ? '' : ' class="' . $formclass . '"'; ?>>
					<input id="sfmoma_ns-nonce" type="hidden" name="sfmoma_ns_nonce" value="<?php echo wp_create_nonce('sfmoma_ns_nonce');?>">
					<input id="sfmoma_ns-name"<?php echo empty($fieldclass) ? '' : ' class="' . $fieldclass . '"'; ?> type="text" name="name" placeholder="Full name">
					<input id="sfmoma_ns-email"<?php echo empty($fieldclass) ? '' : ' class="' . $fieldclass . '"'; ?> type="email" name="email" placeholder="Email">
					<input id="sfmoma_ns-submit"<?php echo empty($fieldclass) ? '' : ' class="' . $fieldclass . '"'; ?> type="submit" value="submit">
				</form>
			<?php
		}
		public static function on_form_submit(){

			// we're using the right action yeah?
			if( $_POST['action'] === 'sfmoma_ns_handle_form' ){
			    
			    // verify nonce
			    if(!wp_verify_nonce( $_POST['nonce_val'], 'sfmoma_ns_nonce' )) {
			    	error_log('invalid nonce');
			        die(-1);
			    }
			    

			    // sanitize name & email
			    $email = sanitize_email( $_POST['email'] );
			    $name = sanitize_text_field( $_POST['name'] );

			    // check email
			    global $wpdb;
			    $results = $wpdb->get_results("SELECT email FROM {$wpdb->prefix}sfmoma_newsletter_signups WHERE email = '${email}'", ARRAY_N);
			    if( !empty($results) ){
			    	echo 'An entry already exists with that email.';
			    	wp_die();
			    }

			    // send email
			    $notify_email = get_site_option( 'sfmoma_ns_notify-email' );
			    if( empty($notify_email) ){
			    	$notify_email = 'publicknowledge@gmail.com';
			    }
			    wp_mail( $notify_email, 'new signup from email form', $name . ' just signed up with the email address: ' . $email );

			    // store entry
			    $wpdb->insert($wpdb->prefix . 'sfmoma_newsletter_signups', array(
			    	'time' => current_time('mysql'),
			    	'name' => $name,
			    	'email' => $email,
			    ), array(
			    	'%s',
			    	'%s',
			    	'%s',
			    ));

			    // generate csv for download
			    @unlink('../wp-content/plugins/sfmoma-newsletter-signup/export' . get_site_option('sfmoma_ns_export-hash') . '.csv');
			    $hash = hash('md5', wp_generate_password());	

			    update_site_option( 'sfmoma_ns_export-hash', $hash );

			    $results = $wpdb->get_results("SELECT time, name, email FROM {$wpdb->prefix}sfmoma_newsletter_signups order by time desc", ARRAY_A);

			    $output_handle = @fopen('../wp-content/plugins/sfmoma-newsletter-signup/export' . $hash . '.csv', 'w');

			    foreach( $results as $result ){
			    	fputcsv( $output_handle, $result );
			    }

			    fseek($output_handle, 0);

			    fclose( $output_handle ); 

			    echo 'Thank you for your submission.';
			    wp_die();
			}
		}
		public static function on_update_email(){
			if( !empty( $_POST['email'] ) ){
				$email = sanitize_email( $_POST['email'] );
				$updated = update_site_option( 'sfmoma_ns_notify-email', $email );
				if( $updated ){
					echo json_encode(1);
				}
				else{
					echo json_encode(0);
				}
			}
			wp_die();
		}
	}
	SFMOMA_NS::init();

	// public wrapper to render form
	function sfmoma_ns_form($formclass = '', $fieldclass = ''){
		SFMOMA_NS::render_signup_form($formclass, $fieldclass);
	}
?>